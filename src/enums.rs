enum ThingsInTheSky {
    // two choices
    Sun(String),
    Stars(String),
}

fn create_skystate(time: i32) -> ThingsInTheSky {
    match time {
        6..=18 => ThingsInTheSky::Sun(String::from("I can see the sun!")),
        _ => ThingsInTheSky::Stars(String::from("I can see the stars!")),
    }
}

fn check_skystate(state: &ThingsInTheSky) {
    match state {
        ThingsInTheSky::Sun(description) => println!("{}", description),
        ThingsInTheSky::Stars(n) => println!("{}", n),
    }
}

enum Mood {
    Happy,
    Sleepy,
    NotBad,
    Angry,
}

// fn match_mood(mood: &Mood) -> i32 {
//     // println!("{:p}", mood); // Print enums reference, Saved the stack at comptime.
//     let happiness_level = match mood {
//         Mood::Happy => 10, // Here we type Mood:: every time
//         Mood::Sleepy => 6,
//         Mood::NotBad => 7,
//         Mood::Angry => 2,
//     };
//     happiness_level
// }

// Shorter version
fn match_mood(mood: &Mood) -> i32 {
    use Mood::*; // import everythings in enums.
    let happiness_level = match mood {
        Happy => 10,
        Sleepy => 6,
        NotBad => 7,
        Angry => 7,
    };
    happiness_level
}

enum Season {
    Spring,
    Summer,
    Autumn,
    Winter,
}

enum Star {
    BrownDwarf = 10,
    RedDwarf = 50,
    YellowDwarf = 100,
    RedGiant = 1000,
    DeadStar, // What number will it have?
}

enum Number {
    // Enums to use multiple types
    U32(u32),
    I32(i32),
}

fn get_number(input: i32) -> Number {
    let number = match input.is_positive() {
        true => Number::U32(input as u32),
        false => Number::I32(input),
    };
    number
}

fn main() {
    // let time = 8;
    // let skystate = create_skystate(time); // Return a ThingsInTheSky
    // check_skystate(&skystate); // Give it a reference so it can read the variable skystate

    // let my_mood = Mood::NotBad;
    // // println!("{}", my_mood == Mood::Happy); // How to enum compair?
    // let happiness_level = match_mood(&my_mood);
    // println!("Out of 1 to 10, my happiness is {}", happiness_level);

    // use Season::*;
    // let four_seasons = vec![Spring, Summer, Autumn, Winter]; // Vec<Season>
    // for season in four_seasons {
    //     println!("{}", season as u32); // Print enum by change type.
    // }

    // use Star::*;
    // let starvec = vec![BrownDwarf, RedDwarf, YellowDwarf, RedGiant];
    // for star in starvec {
    //     match star as u32 {
    //         size if size <= 80 => println!("Not the biggest star."),
    //         size if size >= 80 => println!("This is a good-sized star."),
    //         _ => println!("That star is pretty big!"),
    //     }
    // }
    // println!("What about DeadStar? It's the number {}.", DeadStar as u32);

    let my_vec = vec![get_number(-800), get_number(8)]; // different type values in one vector and faster (because it's same type)

    for item in my_vec {
        match item {
            Number::U32(number) => println!("It's a u32 with the value {}", number),
            Number::I32(number) => println!("It's a i32 with the value {}", number),
        }
    }
}
