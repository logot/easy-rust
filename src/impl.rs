#[derive(Debug)] // Enable Debug print to struct
struct Animal {
    age: u8,
    animal_type: AnimalType,
}

#[derive(Debug)] // Enable Debug print to enum
enum AnimalType {
    Cat,
    Dog,
}

impl Animal {
    // Add functions to 'Animal struct'
    fn new() -> Self {
        // Self means Animal. (alias of type self)
        // you can also write Animal instead of Self
        Self {
            // When we write Animal::new(), we always get a cat that is 10 years old
            age: 10,
            animal_type: AnimalType::Cat,
        }
    }

    fn change_to_dog(&mut self) {
        // (alias of variable self)
        // because we are inside Animal, &mut self means &mut Animal
        // Use .change_to_dog() to change the cat to a dog.
        // wWith &mut sefl we can change it
        println!("Changing animal to dog!");
        self.animal_type = AnimalType::Dog;
    }

    fn change_to_cat(&mut self) {
        //Use .change_to_cat to change the dog to a cat.
        // With &mut self we can change it.
        println!("Changing animal to cat!");
        self.animal_type = AnimalType::Cat;
    }

    fn check_type(&self) {
        match self.animal_type {
            AnimalType::Dog => println!("The animal is a dog"),
            AnimalType::Cat => println!("The animal is a cat"),
        }
    }
}

enum Mood {
    Good,
    Bad,
    Sleepy,
}

impl Mood {
    fn check(&self) {
        match self {
            Mood::Good => println!("Feeling good!"),
            Mood::Bad => println!("Eh, not feeling so good"),
            Mood::Sleepy => println!("Need sleep NOW"),
        }
    }
}

fn main() {
    // println!("{:?}", AnimalType::Cat);
    let mut new_animal = Animal::new(); // Associated function to create a new animal
    new_animal.check_type();
    new_animal.change_to_dog();
    new_animal.check_type();
    new_animal.change_to_cat();
    new_animal.check_type();

    let my_mood = Mood::Sleepy;
    my_mood.check();
}
