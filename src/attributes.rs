#![allow(unused_variables)]
#![allow(dead_code)]

struct JustAStruct {} // dead code

// #[derive(Display)] // Err -> need to describe how to display
#[derive(Debug, PartialEq, Eq, Ord, PartialOrd, Hash, Clone)] // Traits that can be automatically derived
struct HoldsAString {
    the_string: String,
}

// Allow traits for all fields
#[derive(Clone, Copy)] // You also need Clone to use Copy
struct NumberAndBool {
    number: i32,         // i32 is Copy
    true_or_false: bool, // bool is also Copy
}

fn does_nothing(input: NumberAndBool) {}

// fn main() {
//     prints_number(56);
// }

// fn pritns_number(input: i32) {
//     assert_eq!(input % 2, 0); // number must be equal.
//                               // If number % 2 is not 0, it panics
//     println!("The number is not odd. It is {}", input);
// }

//=========================================================
// document of Vec
//=========================================================
fn main() {
    let mut vec = Vec::new();
    vec.push(1);
    vec.push(2);

    assert_eq!(vec.len(), 2); // The vec length is 2
    assert_eq!(vec[0], 1); // vec[0] is 1

    assert_eq!(vec.pop(), Some(2)); // When you use .pop(), you get Some(2)
    assert_eq!(vec.len(), 1); // The vec length is now 1

    vec[0] = 7;
    assert_eq!(vec[0], 7); // Vec[0] is 7

    vec.extend([1, 2, 3].iter().copied()); // copied() : extract value from Some()

    for x in &vec {
        println!("{}", x);
    }
    assert_eq!(vec, [7, 1, 2, 3]); // The vec now has [7, 1, 2, 3]

    let s = "💖💖💖💖💖";
    assert_eq!(s.len(), 20);

    let s = ['💖', '💖', '💖', '💖', '💖'];
    let size: usize = s
        .into_iter()
        .map(|c| std::mem::size_of_val(&c))
        // .inspect(|item| {
        //     print!("{:?}, ", item);
        // })
        .sum();
    assert_eq!(size, 20);

    //=========================================================
    // Attributes
    //=========================================================
    let some_char = '見'; // unused variables
    let some_str = "ひと";

    let my_string = HoldsAString {
        the_string: "Here I am!".to_string(),
    };

    let number_and_bool = NumberAndBool {
        number: 8,
        true_or_false: true,
    };

    does_nothing(number_and_bool);
    does_nothing(number_and_bool); // If it didn't have Copy, this would make an error
}
