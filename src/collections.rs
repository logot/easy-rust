// use std::collections::BTreeMap;
// use std::collections::BTreeSet;
use std::collections::BinaryHeap;
// use std::collections::HashMap;
// use std::collections::HashSet;
use std::collections::VecDeque;

// struct City {
//     name: String,
//     population: HashMap<u32, u32>,
//     // population: BTreeMap<u32, u32>,
// }

// use std::collections::hash_map::Entry;

// pub fn entry(&mut self, key: K) -> Entry<K, V>

// enum Entry::<K, V> {
//     Occupied(OccupiedEntry<K, V>),
//     Vacant(VacantEntry<K, V>),
// }

// fn or_insert(self, default: V) -> &mut V {
//     // Insert the value:V if there is no key.
//     // Why returns mut reference?? - You can use "let" to attach it to a variable, and change the value
//     match self {
//         Occupied(entry) => entry.into_mut(),
//         Vacant(entry) => entry.insert(default),
//     }
// }

fn show_remainder(input: &BinaryHeap<i32>) -> Vec<i32> {
    let mut remainder_vec = vec![];
    for number in input {
        remainder_vec.push(*number);
    }
    remainder_vec
}

fn check_remaining(input: &VecDeque<(&str, bool)>) {
    for item in input {
        if item.1 == false {
            println!("You must: {}", item.0);
        }
    }
}

fn done(input: &mut VecDeque<(&str, bool)>) {
    let mut task_done = input.pop_back().unwrap();
    task_done.1 = true;
    input.push_front(task_done);
}

fn main() {
    // let mut tallinn = City {
    //     name: "Tallinn".to_string(),
    //     population: HashMap::new(),
    //     // population: BTreeMap::new(),
    // };

    // tallinn.population.insert(1372, 2_250);
    // tallinn.population.insert(1851, 24_000);
    // tallinn.population.insert(2020, 437_619);

    // for (year, population) in tallinn.population {
    //     // HashMap<i32, i32> returns a two items each time
    //     println!(
    //         "In the year {} the city of {} had a population of {}.",
    //         year, tallinn.name, population
    //     );
    // }

    //
    // let canadian_cities = vec!["Calgary", "Vancouver", "Gimli"];
    // let german_cities = vec!["Karlsruhe", "Bad Doberan", "Bielefeld"];

    // let mut city_hashmap = HashMap::new();

    // for city in canadian_cities {
    //     city_hashmap.insert(city, "Canada");
    // }
    // for city in german_cities {
    //     city_hashmap.insert(city, "Germany");
    // }

    // println!("{:?}", city_hashmap["Bielefeld"]);
    // println!("{:?}", city_hashmap.get("Bielefeld"));
    // println!("{:?}", city_hashmap.get("Bielefeldd"));

    // let mut book_hashmap = HashMap::new();

    // book_hashmap.insert(1, "L'Allemagne Moderne");
    // book_hashmap.insert(1, "Le Petit Price");
    // book_hashmap.insert(1, "섀도우 오브 유어 스마일");
    // book_hashmap.insert(1, "Eye of the World");

    // println!("{:?}", book_hashmap.get(&1)); // difference with vec - insert(&i32)

    // let mut book_hashmap = HashMap::new();

    // book_hashmap.insert(1, "L'Allemagne Moderne");

    // if book_hashmap.get(&1).is_none() {
    //     book_hashmap.insert(1, "Le Petit Prince");
    // }

    // println!("{:?}", book_hashmap.get(&1));

    // let book_collection = vec![
    //     "L'Allemagne Moderne",
    //     "Le Petit Prince",
    //     "Eye of the World",
    //     "Eye of the World",
    // ];

    // let mut book_hashmap = HashMap::new();

    // for book in book_collection {
    //     // method - to use function on a enum and a struct
    //     // HashMap.entry() : insert key(K) to HashMap, Return Entry<K, V>
    //     // Entry.or_insert : insert value(V) to Entry if key is not exist. Return Entry's &mut value
    //     let return_value = book_hashmap.entry(book).or_insert(0);
    //     *return_value += 1; // Add 1 to Entry's value
    // }

    // for (book, count) in book_hashmap {
    //     println!("Do we have {}? {}", book, count);
    // }

    // let data = vec![
    //     ("male", 9),
    //     ("female", 5),
    //     ("male", 0),
    //     ("female", 6),
    //     ("female", 5),
    //     ("male", 10),
    // ];

    // let mut survey_hash = HashMap::new();

    // for item in data {
    //     survey_hash.entry(item.0).or_insert(Vec::new()).push(item.1);
    // }
    // // HashMap["male":Vec[i32], "female":Vec[i32]]

    // for (male_or_female, numbers) in survey_hash {
    //     println!("{:?}: {:?}", male_or_female, numbers);
    // }

    // let many_numbers = vec![
    //     94, 42, 59, 64, 32, 22, 38, 5, 59, 49, 15, 89, 74, 29, 14, 68, 82, 80, 56, 41, 36, 81, 66,
    //     51, 58, 34, 59, 44, 19, 93, 28, 33, 18, 46, 61, 76, 14, 87, 84, 73, 71, 29, 94, 10, 35, 20,
    //     35, 80, 8, 43, 79, 25, 60, 26, 11, 37, 94, 32, 90, 51, 11, 28, 76, 16, 63, 95, 13, 60, 59,
    //     96, 95, 55, 92, 28, 3, 17, 91, 36, 20, 24, 0, 86, 82, 58, 93, 68, 54, 80, 56, 22, 67, 82,
    //     58, 64, 80, 16, 61, 57, 14, 11,
    // ];

    // // let mut number_hashset = HashSet::new();
    // let mut number_btreeset = BTreeSet::new();

    // for number in many_numbers {
    //     // number_hashset.insert(number); // Do not allow duplicate value
    //     number_btreeset.insert(number); // Ordered!!
    // }

    // let hashset_length = number_hashset.len();
    // println!(
    //     "There are {} unique numbers, so we are missing {}.",
    //     hashset_length,
    //     100 - hashset_length
    // );

    // let mut missing_vec = vec![]; // Vector allow duplicate value
    // for number in 0..100 {
    //     if number_hashset.get(&number).is_none() {
    //         // Option (Some or None) => Check get() returns none
    //         missing_vec.push(number); // Push not contained value in Vector
    //     }
    // }

    // print!("It does not contain: ");
    // for number in missing_vec {
    //     print!("{} ", number);
    // }

    // for entry in number_hashset {
    //     print!("{} ", entry);
    // }

    // for entry in number_btreeset {
    //     print!("{} ", entry);
    // }

    // let many_numbers = vec![0, 5, 10, 15, 20, 25, 30];

    // let mut my_heap = BinaryHeap::new();

    // for number in many_numbers {
    //     my_heap.push(number);
    // }

    // while let Some(number) = my_heap.pop() {
    //     // .pop() returns Some(number) if a number is there, None if not. It pops from the first.
    //     println!(
    //         "Popped off {}. Remaining numbers are: {:?}",
    //         number,
    //         show_remainder(&my_heap)
    //     );
    // }

    // let mut jobs = BinaryHeap::new();

    // jobs.push((100, "Write back to eamil from the CEO"));
    // jobs.push((80, "Finish the report today"));
    // jobs.push((5, "Watch some YouTube"));
    // jobs.push((70, "Tell your team members thanks for always working hard"));
    // jobs.push((30, "Plan who to fire next for the team"));

    // while let Some(job) = jobs.pop() {
    // .pop() order jobs that value in tuple
    //     println!("You need to: {}", job.1);
    // }

    // let mut my_vec = vec![9, 8, 7, 6, 5];
    // my_vec.remove(0); // Remove first item => All the remain item is shift to the left by 1

    let mut my_vec = VecDeque::from(vec![0; 600000]);
    for _ in 0..600000 {
        my_vec.pop_front(); // .pop_front() => Does not require shift!
    }

    let mut my_vecdeque = VecDeque::new();
    let things_to_do = vec![
        "send email to customer",
        "add new product to list",
        "phone Loki back",
    ];

    for thing in things_to_do {
        my_vecdeque.push_front((thing, false));
        ddd
    }

    done(&mut my_vecdeque);
    done(&mut my_vecdeque);

    check_remaining(&my_vecdeque);

    for task in my_vecdeque {
        print!("{:?}", task);
    }
}
