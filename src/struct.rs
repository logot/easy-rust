struct FileDirectory; // unit struct
struct Colour(u8, u8, u8); // tuple struct
struct SizeAndColour {
    // named struct
    size: u32,
    colour: Colour, // Put it in our new named struct
} // No semicolon because there is a whole code block after this.

struct Country {
    population: u32,
    capital: String,
    leader_name: String,
}

fn main() {
    //============= Struct ===============
    // let my_colour = Colour(50, 0, 50);

    // println!("The second part of the colour is: {}", my_colour.1);

    // let size_and_colour = SizeAndColour {
    //     size: 150,
    //     colour: my_colour,
    // };

    // println!("The size is: {}", size_and_colour.size);
    // println!("The colour is: {}", size_and_colour.colour.1);

    //============= ___ ===============
    let population = 500_000;
    let capital = String::from("Elista");
    let leader_name = String::from("Batu Khasikov");

    let kalmykia = Country {
        population,  // population: population,
        capital,     // capital: capital,
        leader_name, // leader_name: leader_name,
    };
}
