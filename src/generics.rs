use std::cmp::PartialOrd; // Lets us can compair
use std::fmt::Debug; // Debug is located at std::fmt::Debug.
use std::fmt::Display;

// fn return_number(number: i32) -> i32 {
//     println!("Here is your number.");
//     number
// } // Normal function

// fn return_number(number: T) -> T { // concrete function
//     println!("Here is your number.");
//     number
// }

fn return_number<T: Debug>(number: T) -> T {
    // <T: Debug> is the important part
    // any type T for this function will have Debug
    println!("Here is your number. {:?}", number);
    number
} // Generic function - Put any type you want

#[derive(Debug)]
struct Animal {
    name: String,
    age: u8,
}

fn print_item<T: Debug>(item: T) {
    println!("Here is your item: {:?}", item);
}

// fn compare_and_display<T: Display, U: Display + PartialOrd>(statement: T, num_1: U, num_2: U) {
//     println!(
//         "{}! is {} greater than {}? {}",
//         statement,
//         num_1,
//         num_2,
//         num_1 > num_2 // Can compair (PartialOrd)
//     );
// }

fn compare_and_display<T, U>(statement: T, num_1: U, num_2: U)
where
    T: Display,
    U: Display + PartialOrd, // More easy to read, especially if you have many types.
{
    println!(
        "{}! is {} greater than {}? {}",
        statement,
        num_1,
        num_2,
        num_1 > num_2 // Can compair (PartialOrd)
    );
}

fn say_two<T: Display, U: Display>(statement_1: T, statement_2: U) {
    println!(
        "I have two things to say: {} and {}",
        statement_1, statement_2
    ); // Type T is a &str, but type U is a String.
}

fn main() {
    // let number = return_number(5);
    // let charlie = Animal {
    //     name: "Charlie".to_string(),
    //     age: 1,
    // };

    // let number = 55;

    // print_item(charlie);
    // print_item(number);

    // compare_and_display("Listen up!", 9, dh); // Not allow different type
    compare_and_display("Listen up!", 9, 8);

    say_two("Hello there!", String::from("I hate sand.")); // Type T is a &str, but type U is a String.
    say_two(
        String::from("Where is Padme?"),
        String::from("Is she all right?"),
    ); // Both types are String.
}
