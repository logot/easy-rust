#![allow(unused_variables)]
#![allow(dead_code)]
use std::ops::Deref;

// All the other code is the same until after the enum Alignment
struct Character {
    name: String,
    strength: u8,
    dexterity: u8,
    health: u8,
    intelligence: u8,
    wisdom: u8,
    charm: u8,
    hit_points: i8,
    alignment: Alignment,
}

impl Character {
    fn new(
        name: String,
        strength: u8,
        dexterity: u8,
        health: u8,
        intelligence: u8,
        wisdom: u8,
        charm: u8,
        hit_points: i8,
        alignment: Alignment,
    ) -> Self {
        Self {
            name,
            strength,
            dexterity,
            health,
            intelligence,
            wisdom,
            charm,
            hit_points,
            alignment,
        }
    }
}

enum Alignment {
    Good,
    Neutral,
    Evil,
}

// We want to keep character hit points in a big vec.
// We'll put monster data in there too, and keep it all together
impl Deref for Character {
    // impl Deref for Character. Now we can do any integer math we want!
    type Target = i8;

    fn deref(&self) -> &Self::Target {
        &self.hit_points
    }
}

fn main() {
    let billy = Character::new("Billy".to_string(), 9, 8, 7, 10, 19, 19, 5, Alignment::Good);
    let brandy = Character::new(
        "Brandy".to_string(),
        9,
        8,
        7,
        10,
        19,
        19,
        5,
        Alignment::Good,
    );

    let mut hit_points_vec = vec![]; // Put our hit points data in here
    hit_points_vec.push(*billy); // Push billy? what things in billy?
    hit_points_vec.push(*brandy); // Push brandy? what things in brandy?
                                  // These are very ambiguous

    println!("{:?}", hit_points_vec);
}
