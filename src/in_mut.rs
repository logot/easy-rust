use std::cell::{Cell, RefCell};
use std::mem::drop;
use std::sync::{Mutex, RwLock};

//=================[Cell]======================
#[derive(Debug)]
struct PhoneModel {
    company_name: String,
    model_name: String,
    screen_size: f32,
    memory: usize,
    date_issued: u32,
    on_sale: Cell<bool>, // Cell that hold the bool
}

//=================[RefCell]======================
#[derive(Debug)]
struct User {
    id: u32,
    year_registered: u32,
    username: String,
    active: RefCell<bool>,
}

fn main() {
    //==============================================
    //=================[Cell]======================
    //==============================================
    let super_phone_3000 = PhoneModel {
        company_name: "YY Electronics".to_string(),
        model_name: "Super Phone 3000".to_string(),
        screen_size: 7.5,
        memory: 4_000_000,
        date_issued: 2020,
        on_sale: Cell::new(true),
    };

    super_phone_3000.on_sale.set(false);

    println!("{:?}", super_phone_3000);

    //==============================================
    //=================[RefCell]======================
    //==============================================
    let user_1 = User {
        id: 1,
        year_registered: 2020,
        username: "User 1".to_string(),
        active: RefCell::new(true),
    };

    println!("{:?}", user_1.active);

    // user_1.active.replace(false);

    let date = 2020;

    user_1
        .active
        .replace_with(|_| if date < 2000 { true } else { false });
    println!("{:?}", user_1.active);

    let borrow_one = user_1.active.borrow_mut();
    // let borrow_two = user_1.active.borrow_mut(); // !!Runtime Error!! => It can't run until first borrowing end.

    //==============================================
    //=================[Mutex]======================
    //==============================================
    let my_mutex = Mutex::new(5); // You don't need to say 'mut'

    {
        let mut mutex_changer = my_mutex.lock().unwrap(); // It has to be mut because it'll change.

        println!("{:?}", my_mutex); // This prints "Mutex { data: <locked> }"
        println!("{:?}", mutex_changer); // => 5
        *mutex_changer += 1; // Why use *? - mutex_changer: MutexGuard<i32>
        println!("{:?}", mutex_changer); // => 6
    } // MutexGuard goes out of scope. => destroy mutex_changer => unlock!!

    println!("{:?}", my_mutex); // This prints "Mutex { data: <locked> }"

    //=================[std::mem::drop()]======================
    let mut mutex_changer = my_mutex.lock().unwrap(); // lock again
    *mutex_changer += 1;
    // std::mem::drop(mutex_changer); // unlock again!
    drop(mutex_changer); // unlock again!
    println!("{:?}", my_mutex);

    //=================[wait for mutex]======================
    let mut mutex_changer = my_mutex.lock().unwrap();
    // let mut other_mutex_changer = my_mutex.lock().unwrap(); // Wait until my_mutex is unlock..
    // println!("This will never print...");
    let mut other_mutex_changer = my_mutex.try_lock(); // Try to get a lock. If can't,
    if let Ok(value) = other_mutex_changer {
        println!("The MutexGuard has: {}", value)
    } else {
        println!("Didn't get the lock")
    }
    drop(mutex_changer);

    //=================[Common mutex]======================
    *my_mutex.lock().unwrap() = 999; // Nobody can access before unlock! I t means unlock my_mutex and make it 999;
    println!("{:?}", my_mutex);

    //==============================================
    //=================[RwLock]======================
    //==============================================
    let my_rwlock = RwLock::new(5);

    let read1 = my_rwlock.read().unwrap(); // one .read() is fine
    let read2 = my_rwlock.read().unwrap(); // two .read()s is also fine

    println!("{:?}, {:?}", read1, read2);
    // let mut write1 = my_rwlock.write().unwrap(); // program will wait forever..

    drop(read1);
    drop(read2);

    let mut write1 = my_rwlock.write().unwrap(); // now this will be run
    *write1 = 6;
    drop(write1);
    println!("{:?}", my_rwlock);

    let my_rwlock = RwLock::new(5);
    let read1 = my_rwlock.read().unwrap();
    let read2 = my_rwlock.read().unwrap();

    if let Ok(mut number) = my_rwlock.try_write() {
        *number += 10;
        println!("now the number is {}", number);
    } else {
        println!("Couldn't get write access, sorry!")
    };
}
