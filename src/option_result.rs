// signature of Option
// enum Option<T> {
//     None,
//     Some(T),
// }

// signature of Result
// enum Result<T, E> {
//     Ok(T), // T : what do you want to return for Ok
//     Err(E), // E : what do you want to return for Err
// }

// fn check_error() -> Result<(), ()> {
//     Ok(())
// }

// fn give_result(input: i32) -> Result<(), ()> {
//     if input % 2 == 0 {
//         return Ok(());
//     } else {
//         return Err(());
//     }
// }

// fn check_if_five(number: i32) -> Result<i32, String> {
//     match number {
//         5 => Ok(number),
//         _ => Err("Sorry, the number wasn't five.".to_string()), // This is our error message
//     }
// }

fn main() {
    // check_error(); // Rust show warning if you don't use Result, because it would be Err.
    // if give_result(5).is_ok() {
    //     println!("It's okay, guys")
    // } else {
    //     println!("It's an error, guys")
    // }
    // let mut result_vec = Vec::new();

    // for number in 2..7 {
    //     result_vec.push(check_if_five(number)); // result_vec: Vec<Result<i32, String>>
    // }

    // println!("{:?}", result_vec);

    // let error_value: Result<i32, &str> = Err("There was an error"); // Create a Result that is already an Err
    // println!("{}", error_value.unwrap()); // Err is can't unwrap.

    let my_vec = vec![2, 3, 4];
    // // .get() : Vec's method that returns Option
    let get_one = my_vec.get(0); // 0 to get the first number
                                 // let get_two = my_vec.get(10); // Returns None
                                 // println!("{:?}", get_one);
                                 // println!("{:?}", get_two);

    // let my_vec = vec![2, 3, 4];

    // for index in 0..10 {
    //     // match my_vec.get(index) {
    //     //     Some(number) => println!("The number is: {}", number),
    //     //     None => {}
    //     // }
    //     if let Some(number) = my_vec.get(index) {
    //         // if let : Do something if it matches, and don't do anything if doesn't
    //         // if somthing => do
    //         // if None => do nothing
    //         println!("The number is: {}", number);
    //     }
    // }

    let weather_vec = vec![
        vec!["Berlin", "cloudy", "5", "-7", "78"],
        vec!["Athens", "sunny", "not humid", "20", "10", "50"],
    ];

    for mut city in weather_vec {
        println!("For the city of {}:", city[0]);
        while let Some(information) = city.pop() {
            // repeat until when city can't be poped. (you can't pop anymore. Return None -> Stop)
            if let Ok(number) = information.parse::<i32>() {
                // Print number if information can be parsed. (Return Result -> Ok() or Err())
                println!("The number is: {}", number);
            }
        }
    }
}
