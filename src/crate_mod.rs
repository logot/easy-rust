#![allow(dead_code)]

mod print_things {
    use std::fmt::{Debug, Display}; // Use the 'use' keyword in mod (because this is a separate space)

    #[derive(Debug)]
    pub struct Billy {
        // Billy is public
        name: String,
        pub times_to_print: u32,
    }

    impl Billy {
        pub fn new(times_to_print: u32) -> Self {
            Self {
                name: "Billy".to_string(), // We choose the name - the user can't
                times_to_print,
            }
        }

        pub fn print_billy(&self) {
            // This function prints a Billy
            for _ in 0..self.times_to_print {
                println!("{:?}", self.name);
            }
        }
    }

    pub fn prints_one_thing<T: Display>(input: T) {
        // Print anything that implements Display
        println!("{}", input);
    }
}

mod country {
    // The main mdo doesn't need pub
    fn print_country(country: &str) {
        // Note: this function isn't public
        println!("We are in the country of {}", country);
    }

    pub mod province {
        fn print_province(province: &str) {
            // Note: this function isn't public
            println!("in the province of {}", province);
        }

        pub mod city {
            use super::super::*; // use everything in "above above"
            use super::*; // use everything in "above"

            // Make this mod public
            pub fn print_city(country: &str, province: &str, city: &str) {
                // A child mod can access to parent mods
                // crate::country::print_country(country);
                // super::super::print_country(country); // super : Brings some item from above (superior)
                print_country(country);

                // crate::country::province::print_province(province);
                // super::print_province(province);
                print_province(province);
                println!("int the city of {}", city);
            }
        }
    }
}

fn main() {
    // crate::print_things::prints_one_thing(6);

    // use crate::print_things::prints_one_thing;
    // use crate::print_things::*; // Imports everything from print_things (* : glob operator)
    // prints_one_thing(6);
    // prints_one_thing("Trying to print a string...".to_string());

    // let my_billy = Billy::new(3);
    // my_billy.print_billy();
    use crate::country::province::city::print_city;

    print_city("Canada", "New Brunswick", "Moncton");
    // print_city can access to country and province, because it's the child of them.
}
