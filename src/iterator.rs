fn iterator_df() {
    let my_vec = vec!['a', 'b', '거', '跨'];

    let mut my_vec_iter = my_vec.iter(); // This is an Iterator type now, but we haven't called it yet

    // assert_eq!() => countinue if it's true
    assert_eq!(my_vec_iter.next(), Some(&'a')); // Call the first item with .next()
    assert_eq!(my_vec_iter.next(), Some(&'b')); // Call the next
    assert_eq!(my_vec_iter.next(), Some(&'거')); // Again
    assert_eq!(my_vec_iter.next(), Some(&'跨')); // Again
    assert_eq!(my_vec_iter.next(), None); // Nothing is left : just None
    assert_eq!(my_vec_iter.next(), None); // You can keep calling .next() but it will always be None
}

fn iterator_ex() {
    let vector1 = vec![1, 2, 3];
    // .iter()
    let vector1_a = vector1.iter().map(|x| x + 1).collect::<Vec<i32>>();
    let vector1_b = vector1.iter();
    let vector1_c = vector1.iter().map(|x| x + 1);
    println!("{:?}", vector1_b);
    println!("{:?}", vector1_c);
    println!("{:?}", vector1_a);

    // .into_iter() => destroy vector1 (because own it)
    let vector1_b = vector1.into_iter().map(|x| x * 10).collect::<Vec<i32>>();
    // .map() : lets you do something to every item, then pass it on.
    let mut vector2 = vec![10, 20, 30];
    // iter_mut() => borrow, it can change value => Return vector2 (don't need .collect() method)
    vector2.iter_mut().for_each(|x| *x += 100); // This is basically just a for loop.
                                                // .for_each() : lets you do something to every item.
                                                // |x| : naming value to x, is called closures

    println!("{:?}", vector1_a);
    println!("{:?}", vector2);
    println!("{:?}", vector1_b);
    // println!("{:?}", vector1); // Can't use, because ownership moved by into_iter()
}

#[derive(Debug)]
struct Library {
    library_type: LibraryType, // this is our enum
    books: Vec<String>,        // list of books
}

#[derive(Debug)]
enum LibraryType {
    // libraries can be city libraries or country libraries
    City,
    Country,
}

impl Library {
    fn add_book(&mut self, book: &str) {
        // we use add_book to add new books
        self.books.push(book.to_string()); // we take a &str and turn it into a String, then add it ot the Vec
    }

    fn new() -> Self {
        // this creates a new Library
        Self {
            library_type: LibraryType::City, // most are in the city so we'll choose City
            books: Vec::new(),
        }
    }
}

impl Iterator for Library {
    type Item = String;

    fn next(&mut self) -> Option<String> {
        match self.books.pop() {
            Some(book) => Some(book + " is found!"), // Rust allows String + &str
            None => None,
        }
    }
}

fn library_ex() {
    let mut my_library = Library::new(); // make a new library
    my_library.add_book("The Doom of the Darksword"); // add some books
    my_library.add_book("Demian - die Geschichte einer Jugend"); // add some books
    my_library.add_book("구운몽");
    my_library.add_book("吾輩は猫である");

    // println!("{:?}", my_library.books); // we can print our Vector of books

    //It failed if library is not an Iterator => You must impl Iterator
    // for item in my_library {
    //     println!("{}", item);
    // }
}

// an iterator which alternates between Some and None
struct Alternate {
    state: i32,
}

impl Iterator for Alternate {
    type Item = i32; // Associated type

    fn next(&mut self) -> Option<i32> {
        let val = self.state;
        self.state = self.state + 1;

        // if it's even, Some(i32), else None
        if val % 2 == 0 {
            Some(val)
        } else {
            None
        }
    }
}

fn main() {
    library_ex();
}
