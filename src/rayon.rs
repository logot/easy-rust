use rayon::prelude::*;
//======================================================
// rayon
//
// EASY concurrency for Rust
//
// .par_iter(), .par_iter_mut(), .par_into_iter() - It means Parallel
//======================================================

fn main() {
    let mut my_vec = vec![0; 200_000];
    // my_vec
    //     .iter_mut() // Vec => Iterator<Vec>, mut for change value in closure
    //     .enumerate() // Iterator<Vec> => Enumerate<Iterator<Vec>>
    //     .for_each(|(index, number)| *number += index + 1); // change the number to index + 1

    my_vec
        .par_iter_mut() // Only changed this => rayon uses threads!! (FASTER)
        .enumerate()
        .for_each(|(index, number)| *number += index + 1);

    println!("{:?}", &my_vec[5000..5005]);
}
