use std::rc::Rc; // import Rc

fn takes_a_string(input: String) {
    println!("It is: {}", input)
}

fn also_takes_a_string(input: String) {
    println!("It is: {}", input)
}

// #[derive(Debug)]
// struct City {
//     name: String,
//     population: u32,
//     city_history: String,
// }

// #[derive(Debug)]
// struct CityData {
//     names: Vec<String>,
//     histories: Vec<String>,
// }

#[derive(Debug)]
struct City {
    name: String,
    population: u32,
    city_history: Rc<String>, // String inside a Rc
}

#[derive(Debug)]
struct CityData {
    names: Vec<String>,
    histories: Vec<Rc<String>>, // A Vec of String inside a Rc
}

fn main() {
    let user_name = String::from("User MacUserson");

    takes_a_string(user_name); // Ownership moved
                               // also_takes_a_string(user_name); // It can't be possible
                               // => So just give it user_name.clone().
                               // => Sometimes it can't be possible either or you don't want to clone it. (value in struct, too long value, etc)
                               // => These are some reasons for Rc => Lets you have more than one owner.

    let calgary = City {
        name: "Calgary".to_string(),
        population: 1_200_000,
        // Pretend that this string is very very long
        // city_history: "Calgary began as a fort called Fort Calgary that...".to_string(),
        city_history: Rc::new("Calgary began as a fort called Fort Calgary that...".to_string()), // make Rc for String => Now city_history can has *many* owner
    };

    let canada_cities = CityData {
        names: vec![calgary.name], // This is using calgary.name, which is short - ownership moved
        histories: vec![calgary.city_history.clone()], // This String is very long to copy or clone
                                   // => So clone String's *reference* using Rc => increase the count
    };

    println!("Calgary's history is: {}", calgary.city_history); // If you don't use Rc, then Error
    println!("{}", Rc::strong_count(&calgary.city_history)); // Print count of owner
    let new_onwer = calgary.city_history.clone();
}
