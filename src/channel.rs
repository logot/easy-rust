use std::sync::mpsc::channel;
use std::thread::spawn;
// use std::sync::mpsc::{channel, Receiver, Sender};

fn main() {
    // let (sender, receiver) = channel();
    // let (sender, receiver): (Sender<i32>, Receiver<i32>) = channel();
    // let sender_clone = sender.clone();
    // let mut handle_vec = vec![];
    // let mut results_vec = vec![];

    // sender.send(5).unwrap();
    // std::thread::spawn(move || {
    //     sender.send("Send a &str this time").unwrap();
    // });

    // std::thread::spawn(move || {
    //     sender_clone.send("And here is another &str").unwrap();
    // });

    // handle_vec.push(std::thread::spawn(move || {
    //     sender.send("Send a &str this time").unwrap();
    // }));

    // handle_vec.push(std::thread::spawn(move || {
    //     sender_clone.send("And here is another &str").unwrap();
    // }));

    // println!("{}", receiver.recv().unwrap()); //recv = receive, not "rec v" (detect someting receive)

    // for _ in handle_vec {
    //     // now handle_vec has 2 items.
    //     // println!("{:?}", receiver.recv().unwrap());
    //     results_vec.push(receiver.recv().unwrap());
    // }

    // println!("{:?}", results_vec);

    let (sender, receiver) = channel(); // create channel
    let hugevec = vec![0; 1_000_000]; // create huge vector
    let mut newvec = vec![]; // Results vector
    let mut handle_vec = vec![]; // ?

    // repeat 10 times
    for i in 0..10 {
        let sender_clone = sender.clone(); // make clone to send many message at same time

        let mut work: Vec<u8> = Vec::with_capacity(hugevec.len() / 10); // make new vec to put the work in. 1/10th the size (100_000)
        work.extend(&hugevec[i * 100_000..(i + 1) * 100_000]); // first part gets 0..100_000, next gets 100_000..200_000, etc

        let handle = spawn(move || {
            for number in work.iter_mut() {
                // do the actual work (same work to many things)
                *number += 1; // add 1 to element
            }
            sender_clone.send(work).unwrap(); // use the sender_clone to send to the work to the receiver.
        });
        handle_vec.push(handle); // push handler into vector
    }

    for handle in handle_vec {
        // wail until the threads are done
        handle.join().unwrap();
    }

    // repeat until reciver can't receive Result
    while let Ok(results) = receiver.try_recv() {
        newvec.push(results); // push the results from receiver.recv() into the vec
    }

    // Vec<Vec<u8>>. To put it together we can use .flatten()
    let newvec = newvec.into_iter().flatten().collect::<Vec<u8>>(); // Now  it's one vec of 1_000_000 u8 numbers

    println!(
        "{:?}, {:?}, total length: {}", // Let's print out some numbers to make sure they are all 1
        &newvec[0..10],                 // show start point ( newvec[0] ~ newvec[9] )
        &newvec[newvec.len() - 10..newvec.len()], // show end point ( newvec[newvec.len()-10] ~ newvec[newvec.len()] )
        newvec.len()                              // And show that the length is 1_000_000 items
    );

    for number in newvec {
        // And let's tell Rust that it can panic if even one number is not 1 ( check results that is right )
        if number != 1 {
            panic!();
        }
    }
}
