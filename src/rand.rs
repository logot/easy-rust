use rand::{thread_rng, Rng};
use std::fmt; // This means the whole crate rand
              // On your computer you can't just write this;
              // you need to write in the Cargo.toml file first

struct Character {
    strength: u8,
    dexterity: u8,    // body quickness
    constitution: u8, // health
    intelligence: u8,
    wisdom: u8,
    charisma: u8, // popularity with people
}

fn three_die_six() -> u8 {
    // A "die" is the thing you throw to get the number
    let mut generator = thread_rng(); // Create our random number generator
    let mut stat = 0; // THis is the total
    for _ in 0..3 {
        stat += generator.gen_range(1..=6); // Add each time
    }
    stat // Return the total
}

fn four_die_six() -> u8 {
    let mut generator = thread_rng();
    let mut results = vec![]; // First put the numbers in a vec
    for _ in 0..4 {
        results.push(generator.gen_range(1..=6));
    }
    results.sort(); // Now a result like [4, 3, 2, 6] becomse [2, 3, 4, 6]
    results.remove(0); // Now it would be [3, 4, 6]
    results.iter().sum() // Return
}

enum Dice {
    Three,
    Four,
}

impl Character {
    fn new(dice: Dice) -> Self {
        match dice {
            Dice::Three => Self {
                strength: three_die_six(),
                dexterity: three_die_six(),
                constitution: three_die_six(),
                intelligence: three_die_six(),
                wisdom: three_die_six(),
                charisma: three_die_six(),
            },
            Dice::Four => Self {
                strength: four_die_six(),
                dexterity: four_die_six(),
                constitution: four_die_six(),
                intelligence: four_die_six(),
                wisdom: four_die_six(),
                charisma: four_die_six(),
            },
        }
    }

    fn display(&self) {
        // We can do this because we implemented Display below
        println!("{}", self);
        println!();
    }
}

impl fmt::Display for Character {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "Your character has these stats:
strenght: {}
dexterrity: {}
constitution: {}
intelligence: {}
wisdom: {}
charisma: {}",
            self.strength,
            self.dexterity,
            self.constitution,
            self.intelligence,
            self.wisdom,
            self.charisma,
        )
    }
}

fn main() {
    // let mut number_maker = thread_rng(); // rng : random number generator
    // for _ in 0..5 {
    //     // let random_u16 = rand::random::<u16>();
    //     // print!("{} ", random_u16);
    //     print!("{} ", number_maker.gen_range(1..11));
    // }

    let weak_billy = Character::new(Dice::Three);
    let strong_billy = Character::new(Dice::Four);
    weak_billy.display();
    strong_billy.display();
}
