// pub fn spawn<F, T>(f: F) -> JoinHandle<T> // f - closures
// where
//     F: FnOnce() -> T,
//     F: Send + 'static,
//     T: Send + 'static,

fn main() {
    // for _ in 0..10 {
    //     let handle = std::thread::spawn(|| {
    //         println!("I am printing something");
    //     });

    //     handle.join(); // Wait for the threads to finish
    // }

    // for _ in 0..1_000_000 {
    //     // make the program declare "let x = 9" one million times
    //     // It has to finish this before it can exit the main function
    //     let _x = 9;
    // } // Silly way..

    // Fn
    let my_string = String::from("I will go into the closure");
    let my_closure = || println!("{}", my_string);

    // FnMut
    let mut my_string = String::from("I will go into the closure");
    let mut my_closure = || {
        my_string.push_str(" now"); // Change value
        println!("{}", my_string);
    };

    // FnOnce
    let my_vec: Vec<i32> = vec![8, 9, 10];
    let my_closure = || {
        my_vec
            .into_iter() // into_iter takes ownership!
            .map(|x| x as u8) // turn it into u8
            .map(|x| x * 2) // multiply by 2
            .collect::<Vec<u8>>() // collect into a Vec
    };
    my_closure();

    // my_closure(); -> Error because my_vec was destoyed.
    let mut my_string = String::from("Can i go inside the thread?");

    let handle = std::thread::spawn(move || {
        // move - You have to move ownership
        println!("{}", my_string); // now my_string is being used as a reference
    });

    // std::mem::drop(my_string); // Try to drop. But the thread stil needs it (async)

    handle.join();
}
