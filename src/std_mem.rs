#![allow(unused_variables)]
#![allow(dead_code)]
#![allow(unused_mut)]
use rand::prelude::*;
use std::convert::TryFrom;
use std::fmt;
use std::ops::Add;

#[derive(Debug, Copy, Clone, PartialEq)] // PartialEq is probably the most important part
struct Point {
    x: i32,
    y: i32,
}
impl Add for Point {
    type Output = Self; // Remember, this is called "associated type" : a "type that goes together"
                        // In this case it's just another Point
    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

#[derive(Clone)] // To reduce writing of lifetimes
struct Country {
    name: String,
    population: u32,
    gdp: u32, // This is the size of the economy
}
impl Country {
    fn new(name: &str, population: u32, gdp: u32) -> Self {
        Self {
            name: name.to_string(),
            population,
            gdp,
        }
    }
}
impl Add for Country {
    // More Traits for calculate => Sub, Mul, Div, AddAssign, SubAssign, MulAssign, DivAssign
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            // Add two Country
            name: format!("{} and {}", self.name, other.name),
            population: self.population + other.population,
            gdp: self.gdp + other.gdp,
        }
    }
}
impl fmt::Display for Country {
    // Run if you println!("{}", Country);
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "In {} are {} people and a GDP of ${}",
            self.name, self.population, self.gdp
        )
    }
}

fn array() {
    let my_cities = ["Beirut", "Tel Aviv", "Nicosia"];

    for city in my_cities {
        // println!("{}", &city); - used before because array doesn't implement Iterator
        println!("{}", city); // Now we can use this!!
    }
    for city in &my_cities {
        println!("{}", city);
    }
    for city in my_cities.iter() {
        println!("{}", city);
    }

    // same as using a tuple in match statements or to get variables from a struct (destructuring)
    let [city1, city2, city3] = my_cities;
    println!("{}", city1);
}

fn char() {
    // You can use .escape_unicode() method to get the Unicode number for a char
    let korean_word = "청춘예찬";
    for character in korean_word.chars() {
        print!("{} ", character.escape_unicode());
    }

    // u8 -> can use From trait (8bit)
    // u32 -> use TryFrom trait becuase it might not work

    let mut random_generator = rand::thread_rng();
    // This wil try 40,000 times to make a char from a u32.
    // This range is 0 (std::u32::MIN) to u32's highest number (std::u32::MAX).
    // If it doesn't work, we will give it '-'.
    for _ in 0..40_000 {
        let bigger_character =
            char::try_from(random_generator.gen_range(std::u32::MIN..std::u32::MAX)).unwrap_or('-'); // inside of prelude
        print!("{}", bigger_character);
    } // you can now get a String from a char String::from(char)
}

fn integer() {
    let some_number = 200_u8;
    let other_number = 200_u8;

    // you can use .checked_add(), .checked_sub(), .checked_mul(), .checked_div() => return Option
    println!("{:?}", some_number.checked_add(other_number)); // u8 max number is 255
    println!("{:?}", some_number.checked_add(1));

    // rhs : rigth hand side => 8 + 3 => 3 : rhs
    let nauru = Country::new("Nauru", 10_670, 160_000_000);
    let vanuatu = Country::new("Vanuatu", 307_815, 820_000_000);
    let micronesia = Country::new("Micronesia", 104_468, 367_000_000);

    // We could have given Country a &str instead of a String for the name. But we would have to write lifetimes everywhere
    // and that would be too much for a small example. Better to just clone them when we call println!
    println!("{}", nauru.clone());
    println!("{}", nauru.clone() + vanuatu.clone());
    println!("{}", nauru + vanuatu + micronesia);
}

fn four_operations(input: f64) {
    println!(
        "For the number {}:
floor: {}
ceiling: {}
rounded: {}
truncated: {}\n",
        input,
        input.floor(), // shift to left
        input.ceil(),  // shift to right
        input.round(), // shift to near
        input.trunc()  // cut the right part of point
    );
}

fn floats() {
    four_operations(9.1);
    four_operations(100.7);
    four_operations(-1.1);
    four_operations(-19.9);

    let my_vec = vec![
        8.0_f64, 7.6, 9.4, 10.0, 22.0, 77.345, 10.22, 3.2, -7.77, -10.0,
    ];

    let maximum = my_vec.iter().fold(f64::MIN, |current_number, next_number| {
        current_number.max(*next_number) // return maximum number
    }); // Note: start with the lowest possible number for an f64.

    let minimum = my_vec.iter().fold(f64::MAX, |current_number, next_number| {
        current_number.min(*next_number) // return minimum number
    }); // And here start with the highest possible number
}

fn bools() {
    let true_false = (true, false);
    println!("{} {}", true_false.0 as u8, true_false.1 as i32);

    let true_false: (i128, u16) = (true.into(), false.into()); // you can use into() if you tell the compiler the type
    println!("{} {}", true_false.0, true_false.1);

    let (tru, fals) = (true.then(|| 8), false.then(|| 8)); // Return Option from bool
    println!("{:?}, {:?}", tru, fals);

    let bool_vec = vec![true, false, true, false, false];

    let option_vec = bool_vec
        .iter()
        .map(|item| {
            item.then(|| {
                println!("Got a {}!", item);
                "It's true, you know" // This goes inside SOme if it's true
            })
        })
        .collect::<Vec<_>>();

    println!("Now we have: {:?}", option_vec);

    // That printed out the Nones too. Let's filter map them out in a new Vec.
    let filtered_vec = option_vec.into_iter().filter_map(|c| c).collect::<Vec<_>>();
    println!("And without the Nones: {:?}", filtered_vec);
}

fn vecs() {
    let mut my_vec = vec![100, 90, 80, 0, 0, 0, 0, 0];
    // my_vec.sort();
    my_vec.sort_unstable(); // Fast sort => doesn't care about the order of numbers if they are the same number last zero to 0
    println!("{:?}", my_vec);

    let mut my_vec = vec!["sun", "sun", "moon", "moon", "sun", "moon", "moon"];
    my_vec.dedup(); // Remove connected duplicating
    println!("{:?}", my_vec);

    // Combine with sort
    my_vec.sort();
    my_vec.dedup();
    println!("{:?}", my_vec);
}

fn strings() {
    // String ~= Vec
    // let mut push_string = String::new();
    let mut push_string = String::with_capacity(4587520); // we know exact memory size, so doesn't need reallocate
    let mut capacity_counter = 0; // capacity starts at 0
                                  // Example of a String that has too many allocations
    for _ in 0..100_000 {
        // Do this 100,000 times
        if push_string.capacity() != capacity_counter {
            // First check if capacity is differenet now
            println!("{}", push_string.capacity()); // If it is, print it
            capacity_counter = push_string.capacity(); // set current capacity
        }
        push_string.push_str("I'm getting pushed into the string!"); // push this in String => occur allocations
    } // Too many reallocate (copy everything over) 18 times.

    push_string.shrink_to_fit(); // We don't want to add string no more, so shrink rest of memory
    println!("{}", push_string.capacity());
    push_string.push('a'); // doubled memory
    println!("{}", push_string.capacity());
    push_string.shrink_to_fit();
    println!("{}", push_string.capacity());

    let mut my_string = String::from(".daer ot drah tib elttil a si gnirts sihT");
    loop {
        let pop_result = my_string.pop(); // read from the end
        match pop_result {
            Some(character) => print!("{}", character),
            None => break,
        }
    }

    let mut my_string = String::from("Age: 20 Height: 194 Wight: 80");
    my_string.retain(|character| character.is_alphabetic() || character == ' '); // Keep if a letter or a space
    dbg!(my_string); // Let's use dbg! for fun this time instead of println!
}

fn os_c_string() {
    let os_string = std::ffi::OsString::from("This string wokrs for you OS too!");
    let my_string = os_string.into_string().unwrap();
    println!("{}", my_string);

    // match os_string.into_string() {
    //     Ok(valid) => valid.thth(),
    //     Err(not_valid) => not_valid.occg(), // You can see that result type is OsString
    // }
}

mod memorys {
    use std::ops::{Deref, DerefMut};
    use std::{fmt, mem};

    struct Ring {
        // Create a ring from Lord of the Rings
        owner: String,
        former_owner: String,
        seeker: String, // seeker means "person looking for it"
    }
    impl Ring {
        fn new(owner: &str, former_owner: &str, seeker: &str) -> Self {
            Self {
                owner: owner.to_string(),
                former_owner: former_owner.to_string(),
                seeker: seeker.to_string(),
            }
        }
    }
    impl fmt::Display for Ring {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            write!(
                f,
                "{} has the ring, {} used to have it, and {} wants it",
                self.owner, self.former_owner, self.seeker
            )
        }
    }

    struct City {
        name: String,
    }
    impl City {
        fn change_name(&mut self, name: &str) {
            let old_name = mem::replace(&mut self.name, name.to_string());
            println!(
                "The city once called {} is now called {}.",
                old_name, self.name
            );
        }
    }

    struct Bank {
        money_inside: u32,
        money_at_desk: DeskMoney, // This is our "smart pointer" type. It has its own default, but it will use u32
    }
    struct DeskMoney(u32); // Create struct to use u32 reference
    impl Default for DeskMoney {
        fn default() -> Self {
            Self(50) // default is always 50, not 0
        }
    }
    impl Deref for DeskMoney {
        // now DeskMoney can access u32's methods
        type Target = u32;

        // define "*" symbol's feature
        fn deref(&self) -> &Self::Target {
            &self.0
        }
    }
    impl DerefMut for DeskMoney {
        // We can add, subtract, etc.
        fn deref_mut(&mut self) -> &mut Self::Target {
            &mut self.0
        }
    }
    impl Bank {
        fn check_money(&self) {
            println!(
                "There is ${} in the back and ${} at the desk.\n",
                self.money_inside,
                *self.money_at_desk // use * (Deref) so we can just print i32
            )
        }
    }

    struct Robber {
        money_in_pocket: u32,
    }
    impl Robber {
        fn check_money(&self) {
            println!("The robber has ${} right now.\n", self.money_in_pocket);
        }

        fn rob_bank(&mut self, bank: &mut Bank) {
            let new_money = mem::take(&mut bank.money_at_desk); // Here it takes the money, and leaves 50 beause that is the default
            self.money_in_pocket += *new_money; // use * because we can only add u32, DeskMoney can't add
            bank.money_inside -= *new_money; // Same here
            println!(
                "She robbed the bank. She now has ${}!\n",
                self.money_in_pocket
            );
        }
    }

    pub fn drops() {
        //=============================================
        // size_of::<_>() & drop()
        //=============================================
        println!("{}", mem::size_of::<i32>()); // print size of i32
        let my_array = [8; 50]; // [8, 8, 8, ..., 8]
        println!("{}", mem::size_of_val(&my_array)); // print size of array i32 * 50
        let mut some_string = String::from("You can drop a String because it's on the heap");
        mem::drop(some_string);
        // some_string.clear(); // If we did this it would panic
    }

    pub fn swaps() {
        //=============================================
        // swap()
        //=============================================
        let mut one_ring = Ring::new("Frodo", "Gollum", "Sauron");
        println!("{}", one_ring);
        mem::swap(&mut one_ring.owner, &mut one_ring.former_owner); // Gollum got the ring back for a second
        println!("{}", one_ring);
    }

    pub fn replaces() {
        //=============================================
        // replace()
        //=============================================
        let mut capital_city = City {
            name: "Constantinople".to_string(),
        };
        capital_city.change_name("Istanbul");
        println!("{}", capital_city.name);
    }

    pub fn takes() {
        //=============================================
        // take()
        //=============================================
        let mut number_vec = vec![8, 7, 0, 2, 49, 9999];
        let mut new_vec = vec![];

        number_vec.iter_mut().for_each(|number| {
            let taker = mem::take(number); // take number and replace with 0
            new_vec.push(taker); // push the taken number to new vector
        });
        println!("{:?}\n{:?}", number_vec, new_vec);
    }

    pub fn memorys() {
        let mut bank_of_klezkavania = Bank {
            money_inside: 5000,
            money_at_desk: DeskMoney(50), // DeskMoney is a smart pointer (pointer to u32) - default is 50
        }; // new Bank
        bank_of_klezkavania.check_money();

        let mut robber = Robber {
            //Set up our robber
            money_in_pocket: 50,
        };
        robber.check_money();

        robber.rob_bank(&mut bank_of_klezkavania); // Rob, then check money
        robber.check_money();
        bank_of_klezkavania.check_money();

        robber.rob_bank(&mut bank_of_klezkavania); // Do it again
        robber.check_money();
        bank_of_klezkavania.check_money();
    }
}

fn main() {
    // array();
    // char();
    // integer();
    // floats();
    // bools();
    // vecs();
    // strings();
    // os_c_string();
    crate::memorys::memorys();
}
