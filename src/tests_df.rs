#![allow(dead_code)]
#![allow(unused_variables)]

fn return_two() -> i8 {
    2
}
// #[test]
// fn it_returns_two() {
//     assert_eq!(return_two(), 2);
// }

fn return_six() -> i8 {
    4 + return_two()
}
// #[test]
// fn it_returns_six() {
//     assert_eq!(return_six(), 6)
// }

const OKAY_CHARACTERS: &str = "1234567890+- "; // Don't forget the space at the end

fn math(input: &str) -> i32 {
    if !input
        .chars()
        .all(|character| OKAY_CHARACTERS.contains(character))
        || !input
            .chars()
            .take(2) // take the start numbers
            .any(|character| character.is_numeric())
    {
        panic!("Please only input numbers, +-, or spaces");
    }

    let input = input
        .trim_end_matches(|x| "+- ".contains(x)) // trim the string from the end until isn't number.
        .chars() // make iterator
        .filter(|x| *x != ' ') // remove ' ' (space)
        .collect::<String>(); // collect to String
                              // Remove + and - at the end, and all spaces (Ensure the end is number)

    let mut result_vec = vec![]; // Results go in here
    let mut push_string = String::new(); // Temp String - This is the string we push in everytime for checking. We will keep reusing it in the loop.

    for character in input.chars() {
        match character {
            '+' => {
                // do not add '+' symbols if push_string is ""
                if !push_string.is_empty() {
                    result_vec.push(push_string.clone()); // push it into the result_vec
                    push_string.clear(); // clear checking string
                }
            }
            '-' => {
                // add '-' symbols until push_string doesn't contain '-'
                if push_string.contains('-') || push_string.is_empty() {
                    push_string.push(character) // just push it into checking string
                } else {
                    // otherwise, it will contain a number
                    result_vec.push(push_string.clone()); // so push the number into result_vec
                    push_string.clear();
                    push_string.push(character); // push the '-' into checking string
                }
            }
            number => {
                // If anything else taht matches.
                if push_string.contains('-') {
                    result_vec.push(push_string.clone()); // push the '-' symbols to result_vec
                    push_string.clear();
                    push_string.push(number); // push the number to checking string
                } else {
                    // otherwise, it's just number.
                    push_string.push(number); // push the number to checing string
                }
            }
        }
    }
    result_vec.push(push_string); // Push one last time after the loop is over.
                                  // Don't need to .clone() because we don't use it anymore

    // result_vec is something like this ["19","--","8"]

    let mut total = 0; // Now it's time to do math.
    let mut adds = true; // select add or sub
    let mut math_iter = result_vec.into_iter();
    while let Some(entry) = math_iter.next() {
        if entry.contains('-') {
            // If it has a - character, check if it's even or odd
            if entry.chars().count() % 2 == 1 {
                adds = match adds {
                    true => false,
                    false => true,
                }; // reverse the adds
                continue;
            } else {
                continue;
            }
        }
        if adds == true {
            total += entry.parse::<i32>().unwrap();
        } else {
            total -= entry.parse::<i32>().unwrap();
            adds = true; // After subtracting, reset adds to true.
        }
    }

    total // return total
}

//======================================================
// Mod for test
//======================================================
#[cfg(test)]
mod tests {
    use super::*; // this module needs to use the function above it.

    // #[test]
    fn it_returns_six() {
        assert_eq!(return_two(), 6);
    }

    // #[test]
    fn it_returns_two() {
        assert_eq!(return_two(), 2);
    }

    #[test]
    fn one_plus_one_is_two() {
        assert_eq!(math("1 + 1"), 2);
    }
    #[test]
    fn one_minus_two_is_munus_one() {
        assert_eq!(math("1 - 2"), -1);
    }
    #[test]
    fn one_minus_minus_one_is_two() {
        assert_eq!(math("1 - -1"), 2);
    }
    #[test]
    fn nine_plus_nine_minus_nine_minus_nine_is_zero() {
        assert_eq!(math("9+9-9-9"), 0);
    }
    #[test]
    fn eight_minus_nine_plus_nine_is_eight_even_with_characters_on_the_end() {
        assert_eq!(math("8   - 9    +9--+-----+++"), 8);
    }
    #[test]
    #[should_panic]
    fn panic_when_characters_not_right() {
        // Here is our new test - it should panic
        math("7 + seven");
    }
}

fn main() {}
