// fn number() -> i32 {
//     8
// }

// fn multiply(number_one: i32, number_two: i32) -> i32 {
//     let result = number_one + number_two;
//     println!("{} times {} is {}", number_one, number_two, result);
//     result
// }

// fn times_two(number: i32) -> i32 {
//     number * 2
//     // number * number
// }

// fn r#return() -> u8 {
//     println!("Here is your number.");
//     8
// }

// fn return_str() -> &str {
//     let country = String::from("Austria");
//     let country_ref = &country;
//     country_ref // String country doesn't available when it's outside of scope.
// }

// fn print_country(country_name: &String) {
//     println!("{}", country_name);
// }

// fn add_hungary(country_name: &mut String) {
//     // first we say that the function takes a mutable reference.
//     country_name.push_str("-Hungary"); // can change value of variable.
//     println!("Now it says: {}", country_name);
// }

// fn adds_hungary(mut country: String) {
//     // adds_hungary takes the String and declares it mutable!
//     // mut country is not reference. It own country now, means can do anything you want.
//     country.push_str("-Hungary");
//     println!("{}", country);
// }

// There is no -> so it's not returning anything
// fn prints_number(number: i32) {
//     // If number was not copy type, it would take it
//     // and we couldn't use it again
//     println!("{}", number);
// }

// fn prints_country(country_name: String) {
//     println!("{}", country_name);
// }

// fn get_length(input: String) {
//     // Takes ownership of a String.
//     println!("It's {} words long.", input.split_whitespace().count());
// }

// fn get_length(input: &String) {
//     println!("It's {} words long.", input.split_whitespace().count());
// }

// fn loop_then_return(mut counter: i32) -> i32 {
//     loop {
//         counter += 1;
//         if counter % 50 == 0 {
//             break;
//         }
//     }
//     counter
// }

fn main() {
    // let my_number = {
    //     let second_number = 8;
    //     second_number + 9
    // };

    // let my_number = (); // immutable variable
    // let mut mut_number = 12i32;
    // mut_number = 35_i32;

    // mut_number = "fe"; // type error ( i32 vs. &str )
    // let my_number = 12.2_f64; // shadowing ( use let )
    // {
    //     let my_number = 9.3; //shadowing2
    //     println!("{}", my_number);
    // }
    // println!("{}", my_number);

    // println!("Hello, world number {}!", my_number); // display printing
    // println!("Hello, world number {:?}!", my_number); // debug printing
    // println!("Hello, world number {:#?}!", my_number);  // pretty printing
    // print!("This will not print a new line!");

    // println!(
    //     "The smallest i8 is {} and the biggest i8 is {}.",
    //     i8::MIN, // evaluate min value
    //     i8::MAX  // evaluate max value
    // );

    // let final_number = {
    //     let y = 10;
    //     let x = 9;
    //     // let x = times_two(x); // x starts at 9
    //     let x = x + y; // shadow with new x: 28
    //     x // return x: final_number is now the value of x
    // };

    // println!("final number: {}", final_number);

    // println!("\t Start with a tab\nand move to a new line");
    // Escape many characters
    // println!(r#"\n with a raw string \" \\" "#);
    // let my_string = "'Ice to see you,' he said.";
    // let quote_string = r#""Ice to see you," he said."#;
    // let hashtag_string = r#"The hashtag #IceToSeeYou to see you,' he said."#;
    // let many_hashtag = r#"You don't have to type ### to use a hashtag."#;
    // println!(
    //     "{}\n{}\n{}\n{}\n",
    //     my_string, quote_string, hashtag_string, many_hashtag
    // );

    // Search for x=9 and add 13
    // println!("name {name}, x = {x}", name = "fred", x = 9.2);

    // let r#let = 6; // The variable's name is let
    // let mut r#mut = 10; // This variable's name is mut
    // println!("{}", r#return());

    // println!("{:?}", b"This will look like numbers"); // format string to ASCII
    //                                                   // * char => byte, string => byte string
    // println!("{:?}", br##"I like to write "#"."##);
    // println!("{}", r##"I ""like to write "#"."##);
    // println!("{:X}", '행' as u32); // Cast char as u32 to get the hexadecimal value
    // println!("\u{D589}, \u{48}, \u{5C45}, \u{3044}"); // Try printing them with unicode escape \u

    // let number = 9;
    // let number_ref = &number;
    // println!("{:p}", number_ref); // print the pointer addreass

    // let number = 555;
    // println!(
    //     "Binary: {:b}, hexadecimal: {:x}, octal: {:o}",
    //     number, number, number
    // ); // print number to variety format

    // let father_name = "Vlad";
    // let son_name = "Adrian Fahrenheit";
    // let family_name = "Tepes";
    // println!(
    //     "This is {1} {2}, son of {0} {2}.",
    //     father_name, son_name, family_name
    // ); // change order by use {index}

    // println!(
    //     "{city1} is in {country} and {city2} is also in {country}, but {city3} is not in {country}.",
    //     city1 = "Seoul", city2 = "Busan", city3 = "Tokyo", country = "Korea"
    // ); // add names to the {}

    // let letter = "a";
    // println!("{test:ㅎ^11.15}, {0}.", letter, test = "t"); // 11 padding 'ㅎ'
    // reading order of compiler - {variable:padding alignment minimum.maximum}
    // alignment - <, ^, >
    // maximum - .number

    // let title = "TODAY'S NEWS";
    // println!("{:-^30}", title);
    // let bar = "|";
    // let content = "C";
    // println!("{: <15}{c}{: >15}", bar, bar, c = content); // mix positioned arguments & named arguments
    // let a = "SEOUL";
    // let b = "TOKYO";
    // println!("{city1:-<15}{city2:->15}", city1 = a, city2 = b);

    // let name = "서태지"; // &str is UTF-8
    // let other_name = String::from("Adrian Fahrenheit Tepes"); // make String using "from"
    // let name = "🤣";
    // println!("My name is actuallly {}", name);

    // println!(
    //     "A String is always {:?} bytes. It is Sized.",
    //     std::mem::size_of::<String>()
    // );

    // println!(
    //     "And an i8 is always {:?} bytes. It is Sized.",
    //     std::mem::size_of::<i8>()
    // );

    // println!(
    //     "And an f64 is always {:?} bytes. It is Sized.",
    //     std::mem::size_of::<f64>()
    // );

    // println!(
    //     "But a &str? It can be anything. '서태지' is {:?} bytes. It is not Sized.",
    //     std::mem::size_of_val("서태지")
    // );

    // println!(
    //     "And 'Adrian fahrenheit Tepes' is {:?} bytes. It is not Sized.",
    //     std::mem::size_of_val("Adrian fahrenheit Tepes")
    // );

    // let my_name = "Billybrobby";
    // let my_pointer = &my_name;
    // let my_country = "USA";
    // let my_home = "Korea";
    // let together = format!(
    //     "I am {} and I come from {} but * live in {}.",
    //     my_name, my_country, my_home
    // );

    // println!("{:?}", std::mem::size_of_val(my_pointer));
    // println!("{:?}", std::mem::size_of_val(&together));

    // let my_string: String = "Try to make this a String".into();
    // println!("{}", my_string);

    // let country = String::from("Austria"); // 24bits in stack
    // let ref_one = &country; // reference toa String (stack mem address)
    // let ref_two = &country;
    // let ref_three = &ref_one;

    // println!("{}", ref_one);
    // println!("{}", ref_three); // print endpoint value, not reference.

    // let mut my_number = 8; // don't forget to write mut here.
    // let num_ref = &mut my_number; // mutable reference to an i32

    // *num_ref += 10; // change value in mutable variable by reference.
    // println!("{}", my_number);

    // let second_number = 800;
    // let triple_reference = &&&second_number;
    // println!(
    //     "Second_number = triple_reference? {}",
    //     second_number == ***triple_reference
    // );

    // println!(
    //     "{:p},{:p},{:p}",
    //     &second_number, &&second_number, &&&second_number
    // ); // makes three references.

    // let mut number = 10;
    // let number_ref = &number; // immutable reference - rule : Don't change anything.
    // let number_change = &mut number; // mutable reference - Occur error, because it effects to immutable variable.
    // *number_change += 10; // conflict with the rules of number_ref (immutable).

    // let mut number = 10;
    // let number_change = &mut number; // mutable reference
    // *number_change += 10;
    // let number_ref = &number; // immutable reference - Not occur error, because nobody effets immutable variable after created.

    // println!("{}", number_ref);

    // let country = String::from("Austria");
    // let country_ref = &country;
    // let country = 8; // shadowing (block previous variable with new one.)
    // println!("{}, {}", country_ref, country); // country_ref still refers to previous country which is not shadowed.

    //========= Error case ==========
    // let country = String::from("Austria");
    // print_country(country); // We print "Austria" (function own String, country destroyed when function end.)
    //                         // let country = print_country(country); // We print "Austria"
    // print_country(country); // That was fun, let's do it again!

    //========= Success case ==========
    // let country = String::from("Austria");
    // print_country(&country); // make reference (function borrow String)
    // print_country(&country);

    // let mut country = String::from("Austria");
    // add_hungary(&mut country); // we also need to give it a mutable reference.

    // let country = String::from("Austria"); // country is not mutable, but we are going to print Austria-Hungary.
    // adds_hungary(country);
    // println!("{}", country); // country is already destroyed.

    // let my_number = 8;
    // prints_number(my_number); // Print 8. prints_number gets a copy of my_number.
    // prints_number(my_number); // Works! (because it's copied and it's on stack)

    // let country = String::from("Kiribati");
    // prints_country(country.clone()); // clone() is a function that returns a copied value. Not gives ownership.
    //                                  // But, String is very large, .clone() use a lot of memory.
    //                                  // So using & for a reference is faster, if you can
    // prints_country(country);
    // prints_country(country); // Error - country is destroyed when previous function ended.

    //======= Using clone() ========
    // let mut my_string = String::new();
    // for _ in 0..50 {
    //     my_string.push_str("Here are some more words "); // push the words on
    //     get_length(my_string.clone()); // gives it a clone every time
    // }

    //======= Using reference ========
    // let mut my_string = String::new();
    // for _ in 0..50 {
    //     my_string.push_str("Here are some more words ");
    //     get_length(&my_string); // gives the reference, doesn't makes clone.
    // }

    //======= Uninitialized variable ========
    // let my_number; // uninitialize - lives until the end of main(). It can be immutable!!

    // {
    //     // Pretend we need to have this code block
    //     let number = {
    //         // Pretend there is code here to make a number
    //         // Lots of code, and finally:
    //         57
    //     };

    //     my_number = loop_then_return(number); // initialize in inside of block
    // }

    // println!("{}", my_number);
    //========= Array ============
    // let array1 = ["One", "Two"]; // [&str; 2]
    // let array2 = ["One", "Two", "Five"]; // [&str; 3] - Different type!

    // Find type by giving it bad instructions.
    // let seasons = ["Spring", "Summer", "Autumn", "Winter"];
    // let seasons2 = ["Spring", "Summer", "Fall", "Autumn", "Winter"];
    // seasons.ddd();
    // seasons2.thd();

    // let my_array = ["a"; 10];
    // println!("{:?}", my_array);

    // let my_numbers = [0, 10, -20];
    // println!("{}", my_numbers[1]);

    // slice
    // let array_of_ten = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

    // // You need to make reference because the compiler doesn't know the size.
    // let three_to_five = &array_of_ten[2..5];
    // let start_at_two = &array_of_ten[1..];
    // let end_at_five = &array_of_ten[..5];
    // let everything = &array_of_ten[..];
    // println!(
    //     "Three to five: {:?}, start at two: {:?}, end at five: {:?}, everything: {:?}",
    //     three_to_five, start_at_two, end_at_five, everything
    // );

    //========= Vector ============
    // let name1 = String::from("Windy");
    // let name2 = String::from("Gomesy");

    // let mut my_vec = Vec::new();
    // If we run the program now, the compiler will give an error.
    // It doesn't know the type of vec.

    // my_vec.push(name1); // Now compiler can know type.
    // my_vec.push(name2);

    // Declare type directly.
    // let mut my_ver: Vec<String> = Vec::new();

    // let mut my_vec = vec![8, 10, 10]; // create vector using macro

    // let vec_of_ten = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    // // Everything is the same as above except we added vec!.
    // let three_to_five = &vec_of_ten[2..10];
    // let start_at_two = &vec_of_ten[1..];
    // let end_at_five = &vec_of_ten[..5];
    // let everything = &vec_of_ten[..];

    // println!(
    //     "Three to five: {:?}, start at two: {:?}, end at five: {:?}, everything: {:?}",
    //     three_to_five, start_at_two, end_at_five, everything
    // );

    //============ Allocation =============
    // One allocation, Two reallocation
    // let mut num_vec = Vec::new();
    // println!("{}", num_vec.capacity()); // 0
    // num_vec.push('a');
    // println!("{}", num_vec.capacity()); // 4
    // num_vec.push('a');
    // num_vec.push('a');
    // num_vec.push('a'); // ..4byte
    // println!("{}", num_vec.capacity()); // 4
    // num_vec.push('a'); // 5byte!
    // println!("{}", num_vec.capacity()); // 8 - doubled

    // Doubled again
    // num_vec.push('a');
    // num_vec.push('a');
    // num_vec.push('a');
    // println!("{}", num_vec.capacity()); // 8
    // num_vec.push('a'); // 9byte!
    // println!("{}", num_vec.capacity()); // 16 - doubled

    // One allocation, No reallocation - Faster!
    // let mut num_vec = Vec::with_capacity(8);
    // num_vec.push('a');
    // println!("{}", num_vec.capacity());
    // num_vec.push('a');
    // println!("{}", num_vec.capacity());
    // num_vec.push('a');
    // println!("{}", num_vec.capacity());
    // num_vec.push('a');
    // num_vec.push('a');
    // println!("{}", num_vec.capacity());

    // Make vector from array
    // let my_vec: Vec<u8> = [1, 2, 3].into();
    // let my_vec2: Vec<_> = [9, 0, 10].into(); // Vec<_> means "choose the Vec type for me"
    // Rust will choose Vec<i32>

    //============ Tuple =============
    //     let random_tuple = ("Here is a name", 8, vec!['a'], 'b', [8, 9, 10], 7.7);
    //     println!(
    //         "Inside the tuple is: First item: {:?}
    // Second item: {:?}
    // Third item: {:?}
    // Fourth item: {:?}
    // Fifth item: {:?}
    // Sixth item: {:?}",
    //         random_tuple.0,
    //         random_tuple.1,
    //         random_tuple.2,
    //         random_tuple.3,
    //         random_tuple.4,
    //         random_tuple.5,
    //     )

    // let str_vec = vec!["one", "two", "three"];
    // // call them a, b (destructing), '_' doesn't makes variables.
    // let (a, b, _) = (str_vec[0], str_vec[1], str_vec[2]);
    // println!("{:?}", b);
}
