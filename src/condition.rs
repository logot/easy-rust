// fn match_colours(rbg: (i32, i32, i32)) {
//     // Doesn't own tuple because it's reference
//     match rbg {
//         (r, _, _) if r < 10 => println!("Not much red"),
//         (_, b, _) if b < 10 => println!("Not much blue"),
//         (_, _, g) if g < 10 => println!("Not much green"),
//         _ => println!("Each colour has at least 10"),
//     }
// }

// fn match_number(input: i32) {
//     match input {
//         number @ 4 => println!(
//             // Get number from input
//             "{} is an unlucky number in China (sounds close to 死)",
//             number
//         ),
//         number @ 13 => println!(
//             "{} is an unlucky number in America, lucky in Italy! In bocca al lupo!",
//             number
//         ),
//         _ => println!("Looks like a normal number"),
//     }
// }

fn main() {
    // let my_number = 5;
    // if my_number == 7 {
    //     // You don't need brackets!
    //     println!("It's seven");
    // } else if my_number == 6 {
    //     println!("It's siz")
    // } else {
    //     println!("It's a different nubmer")
    // }

    // let my_number = 9;
    // if my_number % 2 == 1 && my_number > 0 {
    //     println!("{} is a positive odd number", my_number);
    // } else if my_number == 6 {
    //     println!("It's six")
    // } else {
    //     println!("It's a different number")
    // }

    // let my_number: u8 = 5;
    // // match looks much cleaner - you must match for every possible result
    // match my_number {
    //     0 => println!("it's zero"), // what to do when it match!
    //     1 => println!("it's one"),
    //     2 => println!("it's two"),
    //     _ => println!("It's some other number"), // _ match anything. Occur error if you not given this.
    // }

    // let my_number = 5;
    // let second_number = match my_number {
    //     0 => 0,
    //     5 => 10,
    //     _ => 2,
    // }; //======= allocation based on my_number =======
    // println!("{}", second_number);

    //======= match compicate type ========
    // let sky = "cloudy";
    // let temperature = "warm";

    // match (sky, temperature) {
    //     ("cloudy", "cold") => println!("It's dark and unpleasant today"),
    //     ("clear", "warm") => println!("It's a nice day"),
    //     ("cloudy", "warm") => println!("It's dark but not bad"),
    //     _ => println!("Not sure what the weather is."),
    // }

    //======= match guard ========
    // let children = 5;
    // let married = true;

    // match (children, married) {
    //     (children, married) if married == false =>
    //     // add some condition after matched
    //     {
    //         println!("Not married with {} children", children)
    //     }

    //     (children, married) if children == 0 && married == true => {
    //         println!("Married but no children")
    //     }

    //     _ => println!("Married? {}. Number of children: {}.", married, children),
    // }

    //======= _ ========
    // let first = (200, 0, 0); // it matches only with first one (match => stop)
    // let second = (50, 50, 50);
    // let third = (200, 50, 0);

    // match_colours(first);
    // match_colours(second);
    // match_colours(third);

    // let my_number = 10;
    // let some_variable = match my_number {
    //     10 => 8,
    //     _ => "Not ten", // Different type!!! => Rust don't know it's size.
    // };

    // It's not possible for the same reason.
    // let some_variable = if my_number == 10 { 8 } else { "something else" };

    // let my_number = 10;

    // if my_number == 10 {
    //     let some_variable = 8;
    // } else {
    //     // It's okay because Rust prepair two variables that i32 and &str
    //     let some_variable = "Something else";
    // }

    // match_number(50);
    // match_number(13);
    // match_number(4);
}
