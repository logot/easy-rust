#![allow(unused_variables)]
#![allow(dead_code)]
#![allow(unused_imports)]

use std::error::Error; // To make Error types
use std::fmt::{Display, Formatter};
use std::mem::size_of; // This gives the size of a type

//=======================================================================================
// Apply trait to many things (Enums & Structs)
//=======================================================================================
trait JustATrait {} // We will implement this on everything

enum EnumOfNumbers {
    I8(i8),
    AnotherI8(i8),
    OneMoreI8(i8),
    // TwoMoreI8(i8),
    // ThreeMoreI8(i8),
    // FourMoreI8(i8),
}
impl JustATrait for EnumOfNumbers {}

enum EnumOfOtherTypes {
    I8(i8),
    AnotherI8(i8),
    Collection(Vec<String>),
}
impl JustATrait for EnumOfOtherTypes {}

// Structs are on heap
struct StructOfNumbers {
    an_i8: i8,
    another_i8: i8,
    one_more_i8: i8,
}
impl JustATrait for StructOfNumbers {}

struct StructOfOtherTypes {
    an_i8: i8,
    another_i8: i8,
    a_collection: Vec<String>,
}
impl JustATrait for StructOfOtherTypes {}

struct ArrayAndI8 {
    array: [i8; 1000], // This one will be very large (arrays are on stack)
    an_i8: i8,
    in_u8: u8,
}
impl JustATrait for ArrayAndI8 {}

//=======================================================================================
struct DoesntImplementDisplay {}
struct List {
    item: Option<Box<List>>,
}

impl List {
    fn new() -> List {
        List {
            item: Some(Box::new(List { item: None })),
        }
    }
}
//=======================================================================================
// Error Control
//=======================================================================================
#[derive(Debug)] // Error need Debug trait
struct ErrorOne;

impl Error for ErrorOne {} // Now it is an error type with Debug. Time for Display:

impl Display for ErrorOne {
    // Error need Display, Display need .fmt()
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "You got the first error!") // All it does is write this message
    }
}

#[derive(Debug)]
struct ErrorTwo;

impl Error for ErrorTwo {} //

impl Display for ErrorTwo {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        write!(f, "You got the second error!") // All it does is write this message
    }
}

// Why use Box? => because the result can't be known at compilation time, so just make a reference for Error
// fn returns_errors(input: u8) -> Result<String, dyn Error> {
fn returns_errors(input: u8) -> Result<String, Box<dyn Error>> {
    match input {
        0 => Err(Box::new(ErrorOne)),
        1 => Err(Box::new(ErrorTwo)),
        _ => Ok("Looks fine to me".to_string()),
    }
}

// It only takes something with ~Display~, so it can't accept our struct ~DoesntImplementDisplay~.
fn displays_it<T: Display>(input: T) {
    println!("{}", input);
}

fn just_takes_a_variable<T>(item: T) {} // Takes anythign and drops it.

// fn returns_just_a_trait() -> JustATrait { // JustATrait's size isn't fixed.
// Contain with Box to reference JustATrait. JustATrait stores them on the heap
// fn returns_just_a_trait() -> impl JustATrait { // It can be used, but is too ambiguous.
fn returns_just_a_trait() -> Box<dyn JustATrait> {
    let some_enum = EnumOfNumbers::I8(8);
    // some_enum
    Box::new(some_enum)
}

fn main() {
    // let my_number = 1; // This is an i32
    // just_takes_a_variable(my_number);
    // just_takes_a_variable(my_number); // Using this function twice is no problem, because it's Copy

    // let my_box = Box::new(1); // This is Box<i32>
    // just_takes_a_variable(my_box.clone()); // Without .clone() the second function would make an err
    // just_takes_a_variable(my_box); // because Box is not Copy

    // let my_box = Box::new(1);
    // let an_integer = *my_box;

    // // print same results => smart pointer?
    // println!("{:?}", my_box); // Box<i32>
    // println!("{:?}", an_integer); // i32

    // let mut my_list = List::new();

    // println!(
    //     "{}, {}, {}, {} ,{}",
    //     size_of::<EnumOfNumbers>(),
    //     size_of::<StructOfNumbers>(),
    //     size_of::<EnumOfOtherTypes>(),
    //     size_of::<StructOfOtherTypes>(),
    //     size_of::<ArrayAndI8>(),
    // );

    let vec_of_u8s = vec![0_u8, 1, 80]; // Threee numbers to try out

    for number in vec_of_u8s {
        match returns_errors(number) {
            Ok(input) => println!("{}", input),
            Err(message) => println!("{}", message),
        }
    }
}
