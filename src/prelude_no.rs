#![no_implicit_prelude]
// #![no_std]

extern crate std; // extern crate == use
use std::convert::From;
use std::println;
use std::string::String;
use std::vec;

fn main() {
    let my_vec = vec![8, 9, 10];
    let my_string = String::from("This won't work");
    println!("{:?}, {}", my_vec, my_string);
}
