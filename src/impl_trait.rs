// ===== Impliment Trait! =====
use std::fmt::Display;
// fn gives_higher_i32(one: i32, two: i32) {
//     let higher = if one > two { one } else { two };
//     println!("{} is higher.", higher);
// }

// Allow arguments that is already has Trait
fn gives_higher<T: PartialOrd + Display>(one: T, two: T) {
    let higher = if one > two { one } else { two };
    println!("{} is higher.", higher);
}

// Pretend impl like Generic
fn prints_it(input: impl Into<String> + Display) {
    // Takes anything that can turn into a String and has Display
    println!("You can print many things, including {}", input);
}

// fn map<B, F>(self, f: F) -> Map<Self, F>
// where // Trait bounds - It must have this trait
//     Self: Sized, // size trait
//     F: FnMut(Self::Item) -> B, // FnMut(closure) on Self::Item(value of iterator)
// {
//     Map::new(self, f)
// }

// Return closure using impl
fn returns_a_closure(input: &str) -> impl FnMut(i32) -> i32 {
    match input {
        "double" => |mut number| {
            number *= 2;
            println!("Doubling number. Now it is {}", number);
            number
        },
        "triple" => |mut number| {
            number *= 40;
            println!("Tripling number. Now it is {}", number);
            number
        },
        _ => |number| {
            println!("Sorry, it's the same: {}.", number);
            number
        },
    }
}

enum TimeOfDay {
    // just a simple enum
    Dawn,
    Day,
    Sunset,
    Night,
}

fn change_fear(input: TimeOfDay) -> impl FnMut(f64) -> f64 {
    // The function takes a TimeOfDay. It returns a closure.
    // We use impl FnMut(64) -> f64 to say that it needs to change the value, and also gives the same type back.
    use TimeOfDay::*; // So we only have to write Dawn, Day, Sunset, Night
                      // Instead of TimeOfDay::Dawn, TImeOfDay::Day, etc.
    match input {
        Dawn => |x| {
            // This is the variable character_fear that we give it later
            println!(
                "The morning sun has vanquished the horrible night. You no longer feel afraid."
            );
            println!("Your fear is now {}", x * 0.5);
            x * 0.5
        },
        Day => |x| {
            println!("What a nice day. Maybe put your feet up and rest a bit.");
            println!("Your fear is now {}", x * 0.2);
            x * 0.2
        },
        Sunset => |x| {
            println!("The sun is almost down! This is no good.");
            println!("Your fear is now {}", x * 1.4);
            x * 1.4
        },
        Night => |x| {
            println!("What a horrible night to have a curse.");
            println!("Your fear is now {}", x * 5.0);
            x * 5.0
        },
    }
}

fn main() {
    // gives_higher(8, 10);

    // let name = "Tuon";
    // let string_name = String::from("Tuon");
    // prints_it(name);
    // prints_it(string_name);

    // let my_number = 10;
    // let mut doubles = returns_a_closure("double");
    // let mut triples = returns_a_closure("triple");
    // let mut quadruples = returns_a_closure("quadruple");

    // doubles(my_number);
    // triples(my_number);
    // quadruples(my_number);

    use TimeOfDay::*;
    let mut character_fear = 10.0; // Start Simon with 10

    let mut daytime = change_fear(Day); // Make four closures here to call every time we want to chagne Simon's fear.
    let mut sunset = change_fear(Sunset);
    let mut night = change_fear(Night);
    let mut morning = change_fear(Dawn);

    character_fear = daytime(character_fear); // Call the closures on Simon's fear. They give a message and change the fear number.
                                              // In real life we would have a Character struct and use it as a method instead,
                                              // like this: character_fear.daytime()
    character_fear = sunset(character_fear);
    character_fear = night(character_fear);
    character_fear = morning(character_fear);
}
