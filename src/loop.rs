fn match_colours(rbg: (i32, i32, i32)) {
    println!(
        "Comparing a colour with {} red, {} blue, and {} green:",
        rbg.0, rbg.1, rbg.2
    );
    let new_vec = vec![(rbg.0, "red"), (rbg.1, "blue"), (rbg.2, "green")];
    let mut all_have_at_least_10 = true;
    for item in new_vec {
        if item.0 < 10 {
            all_have_at_least_10 = false;
            println!("Not much {}.", item.1);
        }
    }
    if all_have_at_least_10 {
        println!("Each colour has at least 10.");
    }
    println!();
}

fn main() {
    // let mut counter = 0;
    // let mut counter2 = 0;

    // 'first_loop: loop {
    //     // Can give it a name
    //     counter += 1;
    //     println!("The counter is now: {}", counter);
    //     if counter > 9 {
    //         println!("Now entering the second loop.");

    //         loop {
    //             println!("The second counter is now: {}", counter2);
    //             counter2 += 1;
    //             if counter2 == 3 {
    //                 break 'first_loop;
    //             }
    //         }
    //     }
    // }

    // let mut counter = 0;
    // while counter < 5 {
    //     counter += 1;
    //     println!("The counter is now: {}", counter);
    // }

    // for number in 0..3 {
    //     // exclusive
    //     println!("The number is: {}", number);
    // }

    // for number in 0..=3 {
    //     // inclusive
    //     println!("The next number is: {}", number);
    // }

    // for _number in 0..3 {
    //     // _number means "maybe i will use it later" (different with _)
    //     println!("Printing the same thing three times");
    // }

    // let mut counter = 5;
    // let my_number = loop {
    //     counter += 1;
    //     if counter % 53 == 3 {
    //         break counter; // Return counter using 'break' (because block starts with let)
    //     }
    // };

    // println!("{}", my_number);

    let first = (200, 0, 0);
    let second = (50, 50, 50);
    let third = (200, 50, 0);

    match_colours(first);
    match_colours(second);
    match_colours(third);
}
