#[cfg(test)] // cfg! will know to look for the word test
mod testing {
    use super::*;
    #[test]
    fn check_if_five() {
        assert_eq!(bring_number(true), 5);
    }
}

fn bring_number(should_run: bool) -> u32 {
    if cfg!(test) && should_run {
        // run test and check if it passes
        5
    } else if should_run {
        println!("Returning 5. This is not a test");
        5
    } else {
        println!("This shouldn't run, returning 0.");
        0
    }
}

fn main() {
    // print something depeding on the operating system
    // let helpful_message = if cfg!(target_os = "windows") {
    //     "backslash"
    // } else {
    //     "slash"
    // };
    // println!(
    //     "...then in you hard drive, type the directory name followed by a {}. Then you...",
    //     helpful_message
    // );
    bring_number(true);
    bring_number(false);
}
