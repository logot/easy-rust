#![allow(unused_macros)]
// Macro doesn't compile anything, it just takes an input and gives an output.
// Compiler checks to see if it makes sense.
macro_rules! give_six {
    // Similar with match statement, but input isn't i32
    (6) => {
        6
    };
    () => {
        println!("You didn't give me a number!");
    };
}

macro_rules! might_print {
    (THis is strange input 하하はは哈哈 but it still works) => {
        println!("You guessed the secret message!")
    };
    ($input:expr) => {
        // for an expression, give it the variable name input (use $)
        // it just trys to make it compile, and if it doesn't then it gives an error
        println!("You gave me: {:?}", $input);
    };
    () => {
        println!("You didn't guess it");
    };
}

macro_rules! check {
    ($input1:ident, $input2:expr) => {
        println!(
            "Is {:?} equal to {:?}? {:?}",
            $input1,
            $input2,
            $input1 == $input2
        );
    };
}

macro_rules! print_anything {
    ($input:tt) => {
        // Don't give the space commas, etc.
        let output = stringify!($input); // make it into a string
        println!("{}", output);
    };
    ($($input1:tt), *) => {
        // give the value more than one at a time (* : zero or more)
        // (+ : one or more)
        let output = stringify!($($input),*);
        println!("{}", output);
    };
}

fn main() {
    // let my_number = 10;
    // match my_number {
    //     10 => println!("you got a ten"),
    //     _ => 10, // Err! => match must be return same value
    // }

    // let six = give_six!(6);
    // println!("{}", six);
    // give_six!(); // It doesn't matter what the result is.
    // give_six!(2);

    // might_print!(THis is strange input 하하はは哈哈 but it still works); // Macro just takes an input
    // might_print!(6);
    // might_print!(vec![8, 9, 7, 10]);
    // might_print!();

    // let x = 6;
    // let my_vec = vec![7, 8, 9];
    // check!(x, 6);
    // check!(my_vec, vec![7, 8, 9]);
    // check!(x, 10);

    print_anything!(ththdoeth);
    print_anything!(87575oehq75onth);
    // zero or more
    print_anything!(87575oehq75onth, ntohe, 987987o, 097);
}
