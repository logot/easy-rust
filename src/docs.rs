#![allow(unused)]
use std::cmp::PartialEq;
// Even making a project with nothing can help you learn about traits in Rust.
// here are two structs that do almost nothing, and a fn main() that also does nothing.
// You can add text to docs by write '///' at the start of the line

/// This is a struct that does nothing
struct DoesNothing {}
/// This struct only has one method.
struct PrintThing {}

/// It just prints the same message.
impl PrintThing {
    fn prints_something() {
        println!("I an printing something");
    }
}

#[derive(Debug)]
enum Test {
    A,
    B = 10,
    C,
}

#[test]
fn test_enum() {}

fn main() {
    // assert_eq!(Test::C, 11);
    println!("{:?}", Test::C as i32)
}
