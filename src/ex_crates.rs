#![allow(unused_imports)]
use chrono::prelude::*;
use regex::Regex;
use serde::{Deserialize, Serialize};

//======================================================
// serde
//
// EASY convert to and from formats like JSON, YAML, etc.
//
// The most common way to use it is by creating a struct with two attributes (Serialize, Deserialize)
//======================================================
// regex
//
// Parsing, compiling, and executing regualr expressions.
//
// You can get matches for something like "colour, color, colours and colors" through a sigle search
//======================================================
// chrono
//
// Time functions
//======================================================

#[derive(Serialize, Deserialize, Debug)]
struct Point {
    x: i32,
    y: i32,
}

fn main() {
    let point = Point { x: 1, y: 2 };

    // Convert the Point to a JSON string.
    let serialized = serde_json::to_string(&point).unwrap();

    println!("serialized = {}", serialized);

    // Convert the JSON string back to a Point
    let deserialized: Point = serde_json::from_str(&serialized).unwrap();

    println!("deserialized = {:?}", deserialized);

    //======================================================
    let utc: DateTime<Utc> = Utc::now();
    let local: DateTime<Local> = Local::now();
    println!("{}", utc);
    println!("{}", local);
}
