fn main() {
    let name = "김진호"; // &str - is already borrowed value
    println!("{}", name);
    let new_name = name;
    println!("{}", name);
    println!("{}", new_name);

    let other_name = String::from("마마무");
    let borrowed_name = &other_name;
    // let new_name = other_name; // Can't move ownership because it has been borrowed.
    // println!("{}", other_name); // Not own
    // println!("{}", new_name);
    println!("{}", borrowed_name);
    let new_borrowed_name = borrowed_name; // Does ownership move??
    println!("{}", borrowed_name); // If ownership move to new_borrowed_name, this might be error.
    println!("{}", other_name); // first value is preserved

    let a = String::from("Hello");
    let b = &a; // borrow a.

    let c = a; // ownership is move.
               // println!("{}", b); // borrowed variable already destroyed when ownership was moved.
}
