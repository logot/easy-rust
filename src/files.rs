#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(unused_mut)]
#![allow(dead_code)]
use std::fs;
use std::io::Write;
// pub fn write<P: AsRef<Path>, C: AsRef<[u8]>>(path: P, contents: C) -> Result<()>
// Inside this you give it the file name you want, and the content you want to put in it.
// AsRef<[u8]> is why you can give it either one.
use std::fs::File;
use std::io::Read; // this is to use the function .read_to_string()

use std::fs::OpenOptions;
// pub fn open<P: AsRef<Path>>(path: P) -> io::Result<File>{
//    OpenOptions::new().read(true).open(path.as_ref())
//    OpenOptions::new().write(true).create(true).truncate(true).open(path.as_ref())
// }

fn common_case() -> std::io::Result<()> {
    let mut file = fs::File::create("myfilename.txt")?;
    // Create a file with this name.
    // CAREFUL! If you have a file with this name already,
    // it will delete everything in it.
    // Don't forget the b in front of ". That's because files take bytes.

    // file.write_all(b"Let's put this in the file")?;
    fs::File::create("myfilename.txt")?.write_all(b"Let's put this in the file")?;

    // write
    fs::write("calvin_with_dad.txt", "Calvin: Dad, how come old photographs are always black and white? Didn't they have color...")?;
    let mut calvin_file = File::open("calvin_with_dad.txt")?;
    let mut calvin_string = String::new();
    calvin_file.read_to_string(&mut calvin_string)?;
    calvin_string
        .split_whitespace()
        .for_each(|word| print!("{} ", word.to_uppercase()));
    Ok(())
}

fn option_use() -> std::io::Result<()> {
    let calvin_file = OpenOptions::new()
        .write(true) // to write
        .create_new(true) // to create if there's no file
        .open("calvin_with_dad.txt")?;
    Ok(())
}

fn main() -> std::io::Result<()> {
    fs::write("calvin_with_dad.txt", "Lorem ipsum dolor sit amet, officia excepteur ex fugiat reprehenderit enim labore culpa sint ad nisi Lorem pariatur mollit ex esse exercitation amet. Nisi anim cupidatat excepteur officia. Reprehenderit nostrud nostrud ipsum Lorem est aliquip amet voluptate voluptate dolor minim nulla est proident. Nostrud officia pariatur ut officia. Sit irure elit esse ea nulla sunt ex occaecat reprehenderit commodo officia dolor Lorem duis laboris cupidatat officia voluptate. Culpa proident adipisicing id nulla nisi laboris ex in Lorem sunt duis officia eiusmod. Aliqua reprehenderit commodo ex non excepteur duis sunt velit enim. Voluptate laboris sint cupidatat ullamco ut ea consectetur et est culpa et culpa duis.")?;
    // fs::write remove all the contents if it already exists.
    let mut calvin_file = OpenOptions::new()
        .append(true) // add more contents
        .read(true)
        .open("calvin_with_dad.txt")?;

    calvin_file.write_all(b"And it was a pretty grainy color for a while too.\n")?;

    write!(&mut calvin_file, "That's really weired.\n")?;
    write!(&mut calvin_file, "Well, truth is stranger than fiction.\n")?;
    println!("{}", fs::read_to_string("calvin_with_dad.txt")?);

    Ok(())
}
