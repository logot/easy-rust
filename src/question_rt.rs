// type Result<T> = Result<T, Error>;
use std::num::ParseIntError;

fn give_number(input: &str) -> Result<i32, ParseIntError> {
    input.parse::<i32>() // parse usually can make an error
}

fn main() -> Result<(), ParseIntError> {
    println!("{:?}", give_number("88")?); // this can be error
    println!("{:?}", give_number("5")?);
    Ok(()) // need this to match the error
}
