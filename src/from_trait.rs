use std::fmt::Display;
// use std::convert::From; // Can skip this

fn print_vec<T: Display>(input: &Vec<T>) {
    for item in input {
        print!("{} ", item);
    }
    println!();
}

// #[derive(Debug)]
struct City {
    name: String,
    population: u32,
}
impl City {
    // just for function
    fn new(name: &str, population: u32) -> Self {
        Self {
            name: name.to_string(),
            population,
        }
    }
}
// #[derive(Debug)]
struct Country {
    // Country has many cities
    cities: Vec<City>,
}
impl From<Vec<City>> for Country {
    // Apply From trait to Country and overwrite from()
    fn from(cities: Vec<City>) -> Self {
        Self { cities }
    }
}
impl Country {
    // print cities in Country
    fn print_cities(&self) {
        for city in &self.cities {
            // println!("{:?} has a population of {:?}.", city.name, city.population);
            println!("{} has a population of {}.", city.name, city.population);
        }
    }
}

struct EvenOddVec(Vec<Vec<i32>>); // Only define signature, skip '{}'
impl From<Vec<i32>> for EvenOddVec {
    fn from(input: Vec<i32>) -> Self {
        let mut even_odd_vec: Vec<Vec<i32>> = vec![vec![], vec![]]; // A vec with two empty vecs inside
                                                                    // This is the return value but first we must fill it
        for item in input {
            if item % 2 == 0 {
                even_odd_vec[0].push(item);
            } else {
                even_odd_vec[1].push(item);
            }
        }
        Self(even_odd_vec) // Return it as Self
    }
}

fn main() {
    let array_vec = Vec::from([8, 9, 10]); // Try from an array
    print_vec(&array_vec);

    // let str_vec = Vec::from("What kind of vec will I be?"); // An array from a &str? This will be interesting
    let str_vec = Vec::from("AAAAAA"); // Vec<u8> - types of the &str and the String
    print_vec(&str_vec);

    let string_vec = Vec::from("What kind of vec wil a String be?".to_string());
    print_vec(&string_vec);
    let helsinki = City::new("Helsinki", 631_695);
    let turku = City::new("Turku", 186_795);

    let finland_cities = vec![helsinki, turku]; // Vec<City>
    let finland = Country::from(finland_cities); // Country<Vec<City>>

    finland.print_cities();

    // let bunch_of_numbers = vec![8, 7, -1, 3, 222, 9787, -47, 77, 0, 55, 7, 8];
    // let new_vec = EvenOddVec::from(bunch_of_numbers);

    // println!(
    //     "Even numbers: {:?}\nOdd numbers: {:?}",
    //     new_vec.0[0], new_vec.0[1]
    // ); // EvenOddVec => Vec<Vec<i32>> => struct.index[index]
}
