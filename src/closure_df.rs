// use std::collections::HashMap;

fn main() {
    // let my_closure = || println!("This is a closure");
    // my_closure();

    // let my_closure = |x: i32| println!("{}", x);

    // my_closure(5);
    // my_closure(5 + 5);

    // let my_closure = || {
    //     let number = 7;
    //     let other_number = 10;
    //     println!("The two numbers are {} and {}.", number, other_number);
    //     // This closure can be as long as we want, just like a function.
    // };

    // my_closure();

    // let number_one = 6;
    // let number_two = 10;
    // let my_closure = || println!("{}", number_one + number_two);
    // my_closure();

    // let my_closure = |x: i32| println!("{}", number_one + number_two + x);
    // my_closure(5);

    // let my_vec = vec![8, 9, 10];

    // let fourth = my_vec.get(3).unwrap_or_else(|| {
    //     if my_vec.get(0).is_some() {
    //         &my_vec[0]
    //     } else {
    //         &0
    //     }
    // });

    // println!("{}", fourth);

    // let num_vec = vec![2, 4, 6];
    // let double_vec = num_vec // take num_vec
    //     .iter() // iterate over it
    //     .map(|number| number * 2) // for each item, multifly 2
    //     .collect::<Vec<i32>>(); // then make a new Vec from this
    // println!("{:?}", double_vec);

    // let num_vec = vec![10, 9, 8];

    // num_vec
    //     .iter() // iterate over num_vec
    //     .enumerate() // get (index, number)
    //     .for_each(|(index, number)| println!("Index nubmer {} has number {}", index, number));
    // .map(|(index, number)| println!("Index nubmer {} has number {}", index, number));

    // let some_numbers = vec![0, 1, 2, 3, 4, 5];
    // let some_words = vec!["zero", "one", "two", "three", "four", "five"];

    // let number_word_hashmap = some_numbers
    //     .into_iter()
    //     .zip(some_words.into_iter()) // inside .zip() we put in the other iter.
    //     .collect::<HashMap<_, _>>(); // Rust know it's HashMap<i32, &str>

    // println!(
    //     "For key {} we get {}.",
    //     2,
    //     number_word_hashmap.get(&2).unwrap()
    // );

    // let number_word_hashmap: HashMap<_, _> = some_numbers
    //     .into_iter()
    //     .zip(some_words.into_iter())
    //     .collect();

    // .enumerate() ~= char_indices() : Indices means indexes

    // let numbers_together = "1412347819123452134213921392343";

    // for (index, number) in numbers_together.char_indices() {
    //     match (index % 3, number) {
    //         (0..=1, number) => print!("{}", number), // just print the number if there is a remainder => print(0,1)
    //         _ => print!("{}\t", number), // otherwise print the number with a tab space => print(2\t)
    //     }
    // }

    // let my_vec = vec![8, 9, 10];

    // println!(
    //     "{:?}",
    //     my_vec
    //         .iter()
    //         // .for_each(|| println!("We didn't use the variables at all"))
    //         .for_each(|_| println!("We didn't use the variables at all"))
    // );

    // let months = vec![
    //     "January",
    //     "February",
    //     "March",
    //     "April",
    //     "May",
    //     "June",
    //     "July",
    //     "August",
    //     "September",
    //     "October",
    //     "November",
    //     "December",
    // ];

    // let filtered_months = months
    //     .into_iter() // make an iter
    //     .filter(|month| month.len() < 5) // We don't want months more than 5 bytes in length.
    //     // .len() is fine because each letter is one byte
    //     .filter(|month| month.contains("u")) // Also we only like months with the letter u
    //     .collect::<Vec<&str>>();

    // println!("{:?}", filtered_months);
}
