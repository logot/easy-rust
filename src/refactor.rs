#![allow(dead_code)]
#![allow(unused_variables)]

//=============================================
// make struct instead of separate variables.
//
// a little bit longer but easier to read.
//=============================================
#[derive(Clone)]
struct Calculator {
    results: Vec<String>,  // results_vec => results
    current_input: String, // calculator.current_input => current_input
    total: i32,
    adds: bool,
}

impl Calculator {
    fn new() -> Self {
        Self {
            results: vec![],
            current_input: String::new(),
            total: 0,
            adds: true,
        }
    }

    // implement methods
    fn clear(&mut self) {
        self.current_input.clear();
    }

    fn push_char(&mut self, character: char) {
        self.current_input.push(character);
    }
}

fn return_two() -> i8 {
    2
}

fn return_six() -> i8 {
    4 + return_two()
}

const OKAY_CHARACTERS: &str = "1234567890+- "; // Don't forget the space at the end

fn math(input: &str) -> i32 {
    if !input
        .chars()
        .all(|character| OKAY_CHARACTERS.contains(character))
        || !input
            .chars()
            .take(2) // take the start numbers
            .any(|character| character.is_numeric())
    {
        panic!("Please only input numbers, +-, or spaces");
    }

    let input = input
        .trim_end_matches(|x| "+- ".contains(x)) // trim the string from the end until isn't number.
        .chars() // make iterator
        .filter(|x| *x != ' ') // remove ' ' (space)
        .collect::<String>(); // collect to String
                              // Remove + and - at the end, and all spaces (Ensure the end is number)

    let mut calculator = Calculator::new();

    for character in input.chars() {
        match character {
            '+' => {
                // do not add '+' symbols if calculator.current_input is ""
                if !calculator.current_input.is_empty() {
                    calculator.results.push(calculator.current_input.clone()); // push it into the calculator.results
                    calculator.clear(); // clear checking string
                }
            }
            '-' => {
                // add '-' symbols until calculator.current_input doesn't contain '-'
                if calculator.current_input.contains('-') || calculator.current_input.is_empty() {
                    calculator.push_char(character) // just push it into checking string
                } else {
                    // otherwise, it will contain a number
                    calculator.results.push(calculator.current_input.clone()); // so push the number into calculator.results
                    calculator.clear();
                    calculator.push_char(character); // push the '-' into checking string
                }
            }
            number => {
                // If anything else taht matches.
                if calculator.current_input.contains('-') {
                    calculator.results.push(calculator.current_input.clone()); // push the '-' symbols to calculator.results
                    calculator.clear();
                    calculator.push_char(number); // push the number to checking string
                } else {
                    // otherwise, it's just number.
                    calculator.push_char(number); // push the number to checing string
                }
            }
        }
    }
    calculator.results.push(calculator.current_input); // Push one last time after the loop is over.
                                                       // Don't need to .clone() because we don't use it anymore

    // calculator.results is something like this ["19","--","8"]

    // let mut math_iter = calculator.results.into_iter();
    let math_iter = calculator.results.into_iter();
    // while let Some(entry) = math_iter.next() {
    for entry in math_iter {
        // much simpler then before
        // for loop is actually an iterator
        if entry.contains('-') {
            // If it has a - character, check if it's even or odd
            if entry.chars().count() % 2 == 1 {
                calculator.adds = match calculator.adds {
                    true => false,
                    false => true,
                }; // reverse the calculator.adds
                continue;
            } else {
                continue;
            }
        }
        // if calculator.adds == true {
        if calculator.adds {
            calculator.total += entry.parse::<i32>().unwrap();
        } else {
            calculator.total -= entry.parse::<i32>().unwrap();
            calculator.adds = true; // After subtracting, reset calculator.adds to true.
        }
    }

    calculator.total // return calculator.total
}

//======================================================
// Mod for test
//======================================================
#[cfg(test)]
mod tests {
    use super::*; // this module needs to use the function above it.

    // #[test]
    fn it_returns_six() {
        assert_eq!(return_two(), 6);
    }

    // #[test]
    fn it_returns_two() {
        assert_eq!(return_two(), 2);
    }

    #[test]
    fn one_plus_one_is_two() {
        assert_eq!(math("1 + 1"), 2);
    }
    #[test]
    fn one_minus_two_is_munus_one() {
        assert_eq!(math("1 - 2"), -1);
    }
    #[test]
    fn one_minus_minus_one_is_two() {
        assert_eq!(math("1 - -1"), 2);
    }
    #[test]
    fn nine_plus_nine_minus_nine_minus_nine_is_zero() {
        assert_eq!(math("9+9-9-9"), 0);
    }
    #[test]
    fn eight_minus_nine_plus_nine_is_eight_even_with_characters_on_the_end() {
        assert_eq!(math("8   - 9    +9--+-----+++"), 8);
    }
    #[test]
    #[should_panic]
    fn panic_when_characters_not_right() {
        // Here is our new test - it should panic
        math("7 + seven");
    }
}

fn main() {}
