#![allow(unused_variables)]
#![allow(dead_code)]

#[derive(Debug)]
struct Character {
    name: String,
    age: u8,
    height: u32,
    weight: u32,
    lifestate: LifeState,
    can_use: bool, // Set whether the user can use the character
}

#[derive(Debug)]
enum LifeState {
    Alive,
    Dead,
    NeverAlive,
    Uncertain,
}

impl Character {
    // implement methods
    // fn new(name: String, age: u8, height: u32, weight: u32, alive: bool) -> Self {
    //     Self {
    //         name,
    //         age,
    //         height,
    //         weight,
    //         lifestate: if alive {
    //             // bool -> enum
    //             LifeState::Alive
    //         } else {
    //             LifeState::Dead
    //         },
    //     }
    // }
    fn new() -> Self {
        Self {
            name: "Billy".to_string(),
            age: 15,
            height: 170,
            weight: 70,
            lifestate: LifeState::Alive,
            can_use: true, // .new() always gives a good character, so it's true
        }
    } // .new() must place on first

    // mut self => is not mutation reference
    // take ownership(nobody else can touch it!) => make it mutable
    fn height(mut self, height: u32) -> Self {
        self.height = height;
        self.can_use = false; // If it's change, user can't use the character
        self
    }
    fn weight(mut self, weight: u32) -> Self {
        self.weight = weight;
        self.can_use = false;
        self
    }
    fn name(mut self, name: &str) -> Self {
        self.name = name.to_string();
        self.can_use = false;
        self
    } // change one value and return self

    fn build(mut self) -> Result<Character, String> {
        if self.height < 200 && self.weight < 300 && !self.name.to_lowercase().contains("smurf") {
            self.can_use = true; // Everything is okay, so set to true
            Ok(self) // and return the character
        } else {
            Err("Could not create character. Characters must have:
1) Height below 200
2) Weight below 300
3) A name that is not Smurf (that is a bad word)"
                .to_string())
        }
    }
}

// impl Default for Character {
//     // Make default creation of Character using default() function
//     fn default() -> Self {
//         Self {
//             name: "Billy".to_string(),
//             age: 15,
//             height: 170,
//             weight: 70,
//             lifestate: LifeState::Alive,
//         }
//     }
// }

fn main() {
    // let default_i8: i8 = Default::default();
    // let default_str: String = Default::default();
    // let default_bool: bool = Default::default();

    // println!("'{}', '{}', '{}'", default_i8, default_str, default_bool);

    // let character_1 = Character::new("Billy".to_string(), 15, 170, 70, true);
    // let character_1 = Character::default();
    // Give me a default character but with height 180, weight 80, and name of Skoler
    // let character_1 = Character::default().height(180).weight(80).name("Skoler");
    // We don't want users to be free to create any kind of character anymore
    // let character_1 = Character::new().height(180).weight(80).name("Skoler");

    // println!(
    //     "The character {:?} is {:?} years old.",
    //     character_1.name, character_1.age
    // );

    let character_with_smurf = Character::new().name("Lol I am Smurf!!").build();
    let character_too_tall = Character::new().height(400).build();
    let character_too_heavy = Character::new().weight(500).build();
    let okay_character = Character::new()
        .height(180)
        .weight(80)
        .name("Skoler")
        .build(); // This character is okay. Name is fine, height and weight are fine

    let character_vec = vec![
        character_with_smurf,
        character_too_tall,
        character_too_heavy,
        okay_character,
    ];

    for character in character_vec {
        match character {
            Ok(character_info) => println!("{:?}", character_info),
            Err(err_info) => println!("{:?}", err_info),
        }
        println!();
    }
}
