pub mod somthing {
    pub mod third_mod {
        pub fn print_a_country(input: &mut Vec<&str>) {
            println!(
                "The last country is {} inside the module {}",
                input.pop().unwrap(),
                module_path!()
            );
        }
    }
}
fn main() {
    use somthing::third_mod::*;
    let mut country_vec = vec!["Afghanistan", "Albania", "Algeria", "Andorra"];

    // do some stuff
    println!("Hello from file {}", file!());

    // do some more stuff
    println!(
        "On line {} we got the country {}",
        line!(),
        country_vec.pop().unwrap()
    );

    // do some more stuff
    println!(
        "The next country is {} on line {} and column {}.",
        country_vec.pop().unwrap(),
        line!(),
        column!(),
    );

    // lots more code
    print_a_country(&mut country_vec);
}
