use std::fmt::{Debug, Display};

// fn print_it<T: Display>(input: T) {
//     // T can be too many things.. anythings else with just Display => ambiguous
//     println!("{}", input)
// }

// fn print_it<T: AsRef<str> + Display>(input: T) {
//     println!("{}", input)
// }

fn print_it<T>(input: T)
// Discribe Generic
where
    T: AsRef<str> + Debug + Display, // Add Debug -> Use where -> Because many type
{
    println!("{}", input)
}

fn main() {
    print_it("Please print me");
    print_it("Also, please print me".to_string());
    // print_it(9); // i8 => not str
}
