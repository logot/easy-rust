// use std::num::ParseIntError;

// fn parse_str(input: &str) -> Result<i32, ParseIntError> {
//     let parsed_number = input.parse::<i32>()?; // .parse() => Return Result => Ok(i32) or Err(_)
//                                                // ? => Err() => return Err() and "end".
//                                                // ? => Ok() => go next line.
//     Ok(parsed_number) // Wrap with Ok (type match)
// }

// fn parse_str(input: &str) -> Result<i32, std::num::ParseIntError> {
//     let parsed_number = input.parse::<i32>()?;
//     Ok(parsed_number)
// }

// fn parse_str(input: &str) -> Result<i32, ParseIntError> {
//     let parsed_number = input
//         .parse::<u16>()? // Err => Return
//         .to_string()
//         .parse::<u32>()? // Err => Return
//         .to_string()
//         .parse::<i32>()?; // Err => Return

//     Ok(parsed_number)
// }

// fn prints_three_things(vector: Vec<i32>) {
//     if vector.len() != 3 {
//         // if it is important.
//         panic!("my_vec must always have three items")
//     }
//     println!("{}, {}, {}", vector[0], vector[1], vector[2]);
// }

// fn get_fourth(input: &Vec<i32>) -> i32 {
//     // let fourth = input.get(3).unwrap();
//     let fourth = input.get(3).expect("Input vector needs at least 4 items");
//     *fourth
// }

// fn try_two_unwraps(input: Vec<Option<i32>>) {
//     println!("Index 0 is: {}", input[0].unwrap());
//     println!("Index 1 is: {}", input[1].unwrap());
//     // If prints error, which part is it?? first? or other?
// }

// fn try_two_unwraps(input: Vec<Option<i32>>) {
//     input[0].expect("Index 0 is None");
//     input[1].expect("Index 1 is None");
// }

fn main() {
    // let str_vec = vec!["Seven", "8", "9.0", "nice", "6060"];
    // for item in str_vec {
    //     // Parse &str to i32... => Everythings works?
    //     let parsed = parse_str(item);
    //     println!("{:?}", parsed); // print Result
    // }

    // let failure = "Not a number".parse::<i32>();
    // failure.fdfe(); // ask to compiler what is its type.

    // panic!("Time to panic!");
    // let my_vec = vec![8, 9, 10];
    // let my_vec = vec![8, 9, 10, 10, 55, 99];
    // prints_three_things(my_vec);

    // let my_name = "Loki Laufeyson";

    // assert!(
    //     my_name == "Loki Laufeyson",
    //     "{} should be Loki Laufeyson",
    //     my_name
    // ); // must true
    // assert_eq!(
    //     my_name, "Loki Laufeyson",
    //     "{} and Loki Laufeyson shoud be equal",
    //     my_name
    // ); // must equal
    // assert_ne!(
    //     my_name, "Mithridates",
    //     "You entered {}. Input must be not equal Mithridates",
    //     my_name
    // ); // must not equal
    // let my_vec = vec![9, 0, 10];
    // let fourth = get_fourth(&my_vec);
    // let vector = vec![None, Some(1000)];
    // try_two_unwraps(vector);

    let my_vec = vec![8, 9, 10];
    let fourth = my_vec.get(3).unwrap_or(&0); // If .get doesn't work, we will make the value &0.
                                              // .get returns a reference, so we need &0 and not 0 (type must be same)
                                              // You can write "let *fourth" with a * if you want fourth to be a 0 and not a &0, but here we just print so it doesn't matter
    println!("{}", fourth);
}
