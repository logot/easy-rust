use std::sync::{Arc, Mutex};
use std::thread::spawn;

fn make_arc(number: i32) -> Arc<Mutex<i32>> {
    Arc::new(Mutex::new(number))
}

fn new_clone(input: &Arc<Mutex<i32>>) -> Arc<Mutex<i32>> {
    Arc::clone(&input)
}

fn main() {
    // let handle = std::thread::spawn(|| {
    //     for _ in 0..5 {
    //         println!("The thread is working!") // Just teting the thread
    //     }
    // });

    // handle.join().unwrap(); // Make the thread wait here until it is done
    // println!("Exiting the program");

    // let thread1 = spawn(|| {
    //     for _ in 0..5 {
    //         println!("Thread 1 is working!");
    //     }
    // });

    // let thread2 = spawn(|| {
    //     for _ in 0..5 {
    //         println!("Thread 2 is working!");
    //     }
    // });

    // thread1.join().unwrap();
    // thread2.join().unwrap();
    // println!("Exiting the program");

    //=========================================================
    // With Arc
    //=========================================================
    // let my_number = Arc::new(Mutex::new(0));
    // let my_number1 = Arc::clone(&my_number); // this clone goes into Thread 1
    // let my_number2 = Arc::clone(&my_number); // this clone goes into Thread 2

    // let thread1 = spawn(move || {
    //     // Only the clone goes into Thread 1
    //     for _ in 0..10 {
    //         *my_number1.lock().unwrap() += 1; // Lock the Mutex, change the value
    //     }
    // });

    // let thread2 = std::thread::spawn(move || {
    //     // Only the clone goes into Thread 2
    //     for _ in 0..10 {
    //         *my_number2.lock().unwrap() += 1;
    //     }
    // });

    // thread1.join().unwrap();
    // thread2.join().unwrap();
    // println!("Value is: {:?}", my_number);
    // println!("Exiting the program");

    //=========================================================
    // Common usage of threads
    //=========================================================
    // let my_number = Arc::new(Mutex::new(0));
    // let mut handle_vec = vec![]; // JoinHandles will go in here

    // for _ in 0..2 {
    //     // do this twice
    //     let my_number_clone = Arc::clone(&my_number); // Make the clone before starting the thread

    //     let handle = spawn(move || {
    //         // Put the clone in
    //         for _ in 0..10 {
    //             *my_number_clone.lock().unwrap() += 1;
    //         }
    //     });
    //     handle_vec.push(handle); // save the handle so we can call join on it outside of the loop
    //                              // If we don't push it in the vec, it will just die here (clone of Arc will destroy)
    // }

    // handle_vec
    //     .into_iter()
    //     .for_each(|handle| handle.join().unwrap()); // call join on all handles

    // println!("{:?}", my_number);

    //=========================================================
    // Common usage of threads v2
    //=========================================================
    let mut handle_vec = vec![]; // each handle will go in here
    let my_number = make_arc(0);

    for _ in 0..2 {
        let my_number_clone = new_clone(&my_number);
        let handle = spawn(move || {
            for _ in 0..10 {
                let mut value_inside = my_number_clone.lock().unwrap();
                *value_inside += 1;
            }
        });
        handle_vec.push(handle);
    }

    handle_vec
        .into_iter()
        .for_each(|handle| handle.join().unwrap());

    println!("{:?}", my_number);
}
