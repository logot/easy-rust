struct Item {
    number: u8,
}

impl Item {
    fn compare_number(&self, other_number: u8) {
        // Takes a reference to self
        println!(
            "Are {} and {} equal? {}",
            self.number,
            other_number,
            self.number == other_number
        );
        // We don't need to write *self.number
    }
}

fn main() {
    let my_number = 9;
    let reference = &my_number;

    // println!("{}", my_number == reference);
    // Error occurs because the types are different. ( i32, &i32 )
    println!("{}", my_number == *reference);
    // It works! ( i32, i32 )
    // * : Dereferencing

    let item = Item { number: 8 };
    let reference_number = &item.number; // reference number type is &u8
    println!("{}", *reference_number == 8);
    // println!("{}", reference_number == 8); // Error occurs ( &u8, u8 )

    //=============== dot operator ===============
    let item = Item { number: 10 };
    let reference_item = &item; // referencing struct
    println!("{}", reference_item.number == 8); // Use '.' to dereferencing struct
                                                // You don't need to write *reference_item.number

    //=============== more reference ===============
    let reference_item_two = &reference_item; // &&item
    let reference_item_three = &reference_item_two; //  &&&item
    println!(
        "1: {:p}, 2: {:p}, 3: {:p}",
        reference_item, reference_item_two, reference_item_three
    ); // all different.

    // We dont' need *
    item.compare_number(8);
    reference_item.compare_number(10);
    reference_item_two.compare_number(12); // Get same results => Rust will dereferencing until the end.
    reference_item_three.compare_number(14); // And so on
}
