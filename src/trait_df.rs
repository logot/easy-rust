// #[derive(Debug)] // Common trait - automatically implementing
// struct MyStruct {
//     number: usize,
// }

// struct ThingsToAdd {
//     first_thing: u32,
//     second_thing: f32,
// }

// impl ThingsToAdd {
//     // let result = self.second_thing + self.first_thing as f32;
//     // let result = self.second_thing as u32 + self.first_thing;
//     // or just put the next => fist_thingsecond_thing
// }

// struct Animal {
//     // simple struct - an Animal only has a name
//     name: String,
// }

// trait Dog {
//     // The dog trait gives some functionality
//     fn bark(&self) {
//         println!("Woof?");
//     }
//     fn run(&self); // just write function signature => must overwrite..
// }

// // Apply trait to struct
// impl Dog for Animal {
//     // Change function by overwriting
//     fn bark(&self) {
//         println!("Woof woof!");
//     }

//     // like java's interface and impliment
//     fn run(&self) {
//         // Overwrite run
//         println!("{} is running!", self.name);
//     }
// } // Now Animal has the trait Dog

// // #[derive(Debug)]
// struct Cat {
//     name: String,
//     age: u8,
// }

// use std::fmt;

// impl fmt::Display for Cat {
//     // Display have .to_string() - use format! for .fmt()
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         write!(f, "{} is a cat who is {} years old.", self.name, self.age)
//     }
// }

// fn print_cats(pet: String) {
//     println!("{}", pet);
// }

use std::fmt::Debug; // Used to alias

struct Monster {
    health: i32,
}

#[derive(Debug)]
struct Wizard {
    health: i32,
}

#[derive(Debug)]
struct Ranger {
    health: i32,
}

// trait FightClose: std::fmt::Debug {
//     // A type needs Debug to use FightClose
//     fn attack_with_sword(&self, opponent: &mut Monster) {
//         opponent.health -= 10;
//         println!(
//             "You attack with your sword. Your opponent now has {} health left. You are now at: {:?}", // We can now print self with {:?} because we have Debug
//             opponent.health, &self
//         );
//     }

//     fn attack_with_hand(&self, opponent: &mut Monster) {
//         opponent.health -= 2;
//         println!(
//             "You attack with your soword. Your opponent now has {} ehalth left. You are now at: {:?}",
//             opponent.health, &self
//         );
//     }
// }
// Wizard and Ranger are can both fight up close
// trait FightFromDistance: std::fmt::Debug {
//     // In trait, rust don't know type of &self => Can't use..
//     fn attack_with_bow(&self, opponent: &mut Monster, distance: u32) {
//         // You have to give some distance for attack
//         if distance < 10 {
//             opponent.health -= 10;
//             println!(
//                 "You attack with your bow. Your opponent now has {} health left. You are now at: {:?}",
//                 opponent.health, &self
//             );
//         }
//     }
//     fn attack_with_rock(&self, opponent: &mut Monster, distance: u32) {
//         if distance < 3 {
//             opponent.health -= 4;
//         }
//         println!(
//             "You attack with your rock. Your opponent now has {} health left. You are now at: {:?}",
//             opponent.health, &self
//         );
//     }
// }

trait FightClose {}
trait FightFromDistance {}
trait Magic {} // No methods for any of these traits

impl FightClose for Wizard {}
impl FightClose for Ranger {}
impl FightFromDistance for Ranger {} // Only Ranger can fight up from distance
impl Magic for Wizard {} // Only Wizard gets Magic

fn attack_with_bow<T: FightFromDistance + Debug>(
    character: &T,
    opponent: &mut Monster,
    distance: u32,
) {
    if distance < 10 {
        opponent.health -= 10;
        println!(
            "You attack with your bow. Your opponent now has {} health left. You are now at: {:?}",
            opponent.health, character
        );
    }
}
fn attack_with_sword<T: FightClose + Debug>(character: &T, opponent: &mut Monster) {
    opponent.health -= 10;
    println!(
        "You attack with your sword. Your opponent now has {} health left. You are now at: {:?}",
        opponent.health, character
    );
}
fn fireball<T: Magic + Debug>(character: &T, opponent: &mut Monster, distance: u32) {
    if distance < 15 {
        opponent.health -= 20;
        println!("You raise your hands and cast a fireball! Your opponent now has {} health left. You are now at: {:?}", opponent.health, character);
    }
}

fn main() {
    // let rover = Animal {
    //     name: "Rover".to_string(),
    // };

    // rover.bark(); // Now Animal can use bark()
    // rover.run(); // and it can use run()

    // let mr_mantle = Cat {
    //     name: "Reggie Mantle".to_string(),
    //     age: 4,
    // };

    // // println!("Mr. Mantle is a {:?}", mr_mantle);
    // println!(
    //     "Mr. Mantle's String is {} letters long.",
    //     mr_mantle.to_string().chars().count()
    // );
    // print_cats(mr_mantle.to_string()); // mr_mantle have .to_string()
    //                                    // Display have .fmt()

    let radagast = Wizard { health: 60 };
    let aragorn = Ranger { health: 80 };

    let mut uruk_hai = Monster { health: 40 };

    // radagast.attack_with_sword(&mut uruk_hai);
    // aragorn.attack_with_bow(&mut uruk_hai, 8);

    attack_with_sword(&radagast, &mut uruk_hai);
    attack_with_bow(&aragorn, &mut uruk_hai, 8);
    fireball(&radagast, &mut uruk_hai, 8);
}
