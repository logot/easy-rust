#![allow(unused_variables)]
#![allow(dead_code)]

use std::ops::{Deref, DerefMut};

#[derive(Debug)]
struct HoldsANumber(u8); // all it has so far is Debug..

struct DerefExample<T> {
    value: T,
}

impl<T> Deref for DerefExample<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

impl HoldsANumber {
    fn prints_the_number_times_two(&self) {
        println!("{}", self.0 * 2);
    }
}

// implement Deref for HoldsANumber
impl Deref for HoldsANumber {
    type Target = u8; // Remember, this is the "associated type" : the type that goes together.
                      // You have to use the right type Target = (the type you want to return)

    fn deref(&self) -> &Self::Target {
        // Rust calls .defer() when you use *.
        // We just defined Target as a u8 so this is easy to understand
        &self.0 // We choose &self.0 because it's a tuple struct.
                // In a named struct it would be something like "&self.number"
    }
}

impl DerefMut for HoldsANumber {
    // You don't need type Target = u8;
    // Here because it already knows thanks to Deref

    fn deref_mut(&mut self) -> &mut Self::Target {
        // Everything else is the same except it says mut everywhere
        &mut self.0
    }
}

fn main() {
    // let value = 7; // This is an i32
    // let reference = &7; // This is a &i32
    //                     // println!("{}", value == reference); // Cannot be compaired
    // println!("{}", value == *reference); // Use it by dereferencing

    // let my_number = HoldsANumber(20);
    // // println!("{}", my_number.0 + 20); // It just adding a separate u8 to the 20
    // println!("{}", *my_number + 20); // It can't used this until impl Deref

    // // The message cannot be deference
    // // We need to implement Deref. -> smart pointer
    // let x = DerefExample { value: 'a' };
    // assert_eq!('a', *x); // it can be derefed!

    // println!("{:?}", my_number.checked_sub(100)); // This method comes from u8
    //                                               // Note that it's the HoldsANumber!!
    // my_number.prints_the_number_times_two(); // This is our sonw method

    let mut my_number = HoldsANumber(20);
    *my_number = 30; //DerefMut lets us do this
    println!("{:?}", my_number.checked_sub(100));
    my_number.prints_the_number_times_two();
}
