// struct Company {
//     name: String,
//     ceo: Option<String>,
// }

// impl Company {
//     fn new(name: &str, ceo: &str) -> Self {
//         let ceo = match ceo {
//             "" => None,
//             ceo => Some(ceo.to_string()),
//         }; // ceo is decided, so now we return Self
//         Self {
//             name: name.to_string(),
//             ceo,
//         }
//     }

//     fn get_ceo(&self) -> Option<String> {
//         self.ceo.clone() // Just returns a clone of the CEO (struct is not Copy)
//     }
// }

// fn in_char_vec(char_vec: &Vec<char>, check: char) {
//     println!(
//         "Is {} inside? {}",
//         check,
//         char_vec.iter().any(|&char| char == check) // any one item
//     );
// }

#[derive(Debug)]
struct Names {
    one_word: Vec<String>,
    two_words: Vec<String>,
    three_words: Vec<String>,
}

fn main() {
    //=========================================================
    // .filter_map() => filter + map
    //=========================================================
    // let company_vec = vec![
    //     Company::new("umbrella Corporation", "Unknown"),
    //     Company::new("Ovintiv", "Doug Suttles"),
    //     Company::new("The Red-Headed League", ""),
    //     Company::new("Stark Enterprises", ""),
    // ];

    // let all_the_ceos = company_vec
    //     .into_iter()
    //     .filter_map(|company| company.get_ceo()) // filter_map needs Option => Return its value of nothing
    //     .collect::<Vec<String>>();

    // println!("{:?}", all_the_ceos);

    //=========================================================
    // .ok()
    //=========================================================
    // let user_input = vec![
    //     "8.9",
    //     "Nine point nine five",
    //     "8.0",
    //     "7.6",
    //     "eleventy-twelve",
    // ];

    // let actual_numbers = user_input
    //     .into_iter()
    //     .filter_map(|input| input.parse::<f32>().ok()) // Turn Result to Option by .ok()
    //     .collect::<Vec<f32>>();

    // println!("{:?}", actual_numbers);

    //=========================================================
    // .ok_or() & .ok_or_else()
    //=========================================================
    // let mut results_vec = vec![]; // Pretend we need to gather error results too

    // company_vec
    //     .iter()
    //     .for_each(|company| results_vec.push(company.get_ceo().ok_or("No CEO found"))); // Return Result by .ok_or()

    // company_vec.iter().for_each(|company| {
    //     results_vec.push(company.get_ceo().ok_or_else(|| {
    //         let err_message = format!("No CEO found for {}", company.name); // more detail for Err
    //         err_message
    //     }))
    // });

    //     for item in results_vec {
    //         println!("{:?}", item);
    //     }

    //=========================================================
    // .and_then()
    //=========================================================
    // let new_vec = vec![8, 9, 0];

    // let number_to_add = 5;
    // let mut empty_vec = vec![];

    // for index in 0..5 {
    //     empty_vec.push(
    //         new_vec
    //             .get(index) // return Option
    //             .and_then(|number| Some(number + 1)) // If result is Some then run function and return Option(value). If result is None, then return None
    //             .and_then(|number| Some(number + number_to_add)),
    //     );
    // }
    // println!("{:?}", empty_vec);

    //=========================================================
    // .and()
    //=========================================================
    // let one = true;
    // let two = false;
    // let three = true;
    // let four = true;

    // println!("{}", one && three); // return true ( true1 && true2 => true2 )
    // println!("{}", one && two && three && four); // return false

    // let first_try = vec![
    //     Some("success!"),
    //     None,
    //     Some("success!"),
    //     Some("success!"),
    //     None,
    // ];
    // let second_try = vec![
    //     None,
    //     Some("success!"),
    //     Some("success!"),
    //     Some("success!"),
    //     Some("success!"),
    // ];
    // let third_try = vec![
    //     Some("success!"),
    //     Some("success!"),
    //     Some("success!"),
    //     Some("success!"),
    //     None,
    // ];

    // for i in 0..first_try.len() {
    //     println!("{:?}", first_try[i].and(second_try[i]).and(third_try[i]));
    // }

    //=========================================================
    // .all()
    //=========================================================
    // let char_vec = ('a'..'働').collect::<Vec<char>>();
    // in_char_vec(&char_vec, 'i');
    // in_char_vec(&char_vec, '뷁');
    // in_char_vec(&char_vec, '鑿');

    // let smaller_vec = ('A'..'z').collect::<Vec<char>>();
    // println!(
    //     "All alphabetic? {}",
    //     smaller_vec.iter().all(|&x| x.is_alphabetic()) // if all match then return true
    // );
    // println!(
    //     "All less than the character 행? {}",
    //     smaller_vec.iter().all(|&x| x < '행')
    // );

    //=========================================================
    // .rev()
    //=========================================================
    // let mut big_vec = vec![6; 1000];
    // big_vec.push(5);

    // let mut counter = 0;
    // let mut big_iter = big_vec.into_iter(); // destroy vec
    // let mut big_iter = big_vec.into_iter().rev();

    // loop {
    //     counter += 1;
    //     if big_iter.next() == Some(5) {
    //         break;
    //     }
    // }
    // println!("Final counter is: {}", counter);

    // let mut iterator = big_vec.iter().rev();
    // println!("{:?}", iterator.next());
    // println!("{:?}", iterator.next());

    // println!("{:?}", big_vec.iter().rev().any(|&number| number == 5));

    //=========================================================
    // .find() & .position()
    //=========================================================
    // let num_vec = vec![10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
    // println!("{:?}", num_vec.iter().find(|&number| number % 3 == 0));
    // println!("{:?}", num_vec.iter().find(|&number| number % 2 == 30));

    // println!("{:?}", num_vec.iter().position(|&number| number % 3 == 0));
    // println!("{:?}", num_vec.iter().position(|&number| number % 2 == 30));

    //=========================================================
    // .cycle()
    //=========================================================
    // let even_odd = vec!["even", "odd"];

    // // Zip up - two iterators into a single iterator of pairs.
    // let even_odd_vec = (0..6)
    //     .zip(even_odd.into_iter().cycle()) // .cycle() - loop forever, doesn't get next()
    //     // .zip() intercept loop when first iterator is done
    //     .collect::<Vec<(i32, &str)>>();

    // println!("{:?}", even_odd_vec);

    // let ten_chars = ('a'..).take(10).collect::<Vec<char>>();
    // let skip_then_ten_chars = ('a'..).skip(1300).take(10).collect::<Vec<char>>();
    // println!("{:?}", ten_chars);
    // println!("{:?}", skip_then_ten_chars);

    //=========================================================
    // .fold()
    //=========================================================
    // let some_numbers = vec![9, 6, 9, 10, 11];

    // println!(
    //     "{}",
    //     some_numbers
    //         .iter()
    //         .fold(0, |total_so_far, next_number| total_so_far + next_number) // start from 0, use closure until the end
    // ); // fist arg (total_so_far) : return of previous closure
    //    // second arg (next_number): elememt of iterator

    // let a_string = "I don't have any dashes in me.";

    // println!(
    //     "{}",
    //     a_string
    //         .chars()
    //         .fold("-".to_string(), |mut string_so_far, next_char| {
    //             string_so_far.push(next_char);
    //             string_so_far.push('-');
    //             string_so_far
    //         })
    // );

    //=========================================================
    // Others..
    //=========================================================
    //=================[.chunks() & .windows()]======================
    // let num_vec = vec![1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

    // for chunk in num_vec.chunks(3) {
    //     println!("{:?}", chunk);
    // }
    // println!();
    // for window in num_vec.windows(3) {
    //     println!("{:?}", window);
    // }

    //=================[.match_indices()]======================
    // let rules = "Rule number 1: No fighting. Rule number 2: Go to bed at 8 pm. Rule nubmer 3: Wake up at 6 am.";
    // let rule_locations = rules.match_indices("Rule").collect::<Vec<(_, _)>>();
    // println!("{:?}", rule_locations);

    //=================[.peekable() & .peek()]======================
    // let just_numbers = vec![1, 5, 100];
    // let mut number_iter = just_numbers.iter().peekable(); // look at next element without consuming

    // for _ in 0..3 {
    //     println!("I love the number {}", number_iter.peek().unwrap()); // not consuming, just peek!!
    //     println!("I really love the number {}", number_iter.peek().unwrap());
    //     println!("{} is such a nice number.", number_iter.peek().unwrap());
    //     number_iter.next(); // consuming => move to next element
    // }

    // let locations = vec![
    //     ("Nevis", 25),
    //     ("Taber", 8428),
    //     ("Markerville", 45),
    //     ("Cardstom", 3585),
    // ];
    // let mut location_iter = locations.iter().peekable();
    // while location_iter.peek().is_some() {
    //     // if next value is_some, advances following..
    //     match location_iter.peek() {
    //         Some((name, number)) if *number < 100 => {
    //             // .peak() gives us a reference so we need *
    //             println!("Found a hamlet: {} with {} people", name, number)
    //         }
    //         Some((name, number)) => println!("Found a town: {} wtih {} people", name, number),
    //         None => break,
    //     }
    //     location_iter.next();
    // }

    let vec_of_names = vec![
        "Caesar",
        "Frodo Baggins",
        "Bilbo Baggins",
        "Jean-Luc Picard",
        "Data",
        "Rand Al'Thor",
        "Paul Atreides",
        "Barack Hussein Obama",
        "Bill Jefferson Clinton",
    ];

    let mut iter_of_names = vec_of_names.iter().peekable();

    let mut all_names = Names {
        one_word: vec![],
        two_words: vec![],
        three_words: vec![],
    };

    while iter_of_names.peek().is_some() {
        // repeat until there's not next value
        let next_item = iter_of_names.next().unwrap(); // next() => moves to next
        match next_item.match_indices(' ').collect::<Vec<_>>().len() {
            0 => all_names.one_word.push(next_item.to_string()),
            1 => all_names.two_words.push(next_item.to_string()),
            _ => all_names.three_words.push(next_item.to_string()),
        }
    }

    println!("{:?}", all_names);
}
