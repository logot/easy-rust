// Shape of Option
// enum Option<T> {
//     None,
//     Some(T),
// }

// fn take_fifth(value: Vec<i32>) -> i32 {
//     value[4]
// }
// there is no fifth element! - it's panic
// Panic means that the program stops before the problem happens.
// Unwinds the stack -> "Sorry, I can't do that"

fn take_fifth(value: Vec<i32>) -> Option<i32> {
    if value.len() < 5 {
        None
    } else {
        Some(value[4]) // Wrapped in an Option
    }
}

fn handle_option(my_option: Vec<Option<i32>>) {
    // Use match to check Option
    for item in my_option {
        match item {
            Some(number) => println!("Found a {}!", number),
            None => println!("Found a None"),
        }
    }
}

fn main() {
    let new_vec = vec![1, 2];
    let bigger_vec = vec![1, 2, 3, 4, 5];
    // let mut option_vec = Vec::new(); // Make a new vec to hold our options
    //                                  // The vec is type: Vec<Option<i32>>. That means a vec of Option<i32>
    //                                  // let index = take_fifth(new_vec);

    // option_vec.push(take_fifth(new_vec)); // When data pushed in option_vec, Rust compiler will know its type
    // option_vec.push(take_fifth(bigger_vec));
    // handle_option(option_vec);

    // println!(
    //     "{:?}, {:?}",
    //     // take_fifth(new_vec).unwrap(), - Use unwrap if you are sure (You MUST. None.unwrap() -> panic!)
    //     take_fifth(new_vec),
    //     take_fifth(bigger_vec).unwrap(),
    // );

    let vec_of_vecs = vec![new_vec, bigger_vec];
    for vec in vec_of_vecs {
        let inside_number = take_fifth(vec);
        if inside_number.is_some() {
            // .is_some() returns true if we get Some, false if we get None
            println!("We got: {}", inside_number.unwrap()); // now it is safe to use .unwrap() because we already checked
        } else {
            println!("We got nothing.");
        }
    }
}
