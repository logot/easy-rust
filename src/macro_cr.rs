#![allow(dead_code)]
#![allow(unused_macros)]
macro_rules! make_a_function {
    ($name:ident, $($input:tt), *) => {
        fn $name() {
            let output = stringify!($($input), *);
            println!("{}", output);
        }
    };
}

macro_rules! write {
    ($dst:expr, $($arg:tt)*) => ($dst.write_fmt($crate::format_args!($($arg)*)))
    // 1. an expression (expr) that gets the variable name $dts
    // 2. we take zero, one or more arguments -> $($arg:tt)*
    // 3. takes $dst and uses a method called write_fmt.
    // - Inside this method, it uses another macro called format_args that takes all $($arg)*
}

macro_rules! todo_rep {
    () => (panic!("not yet implemented"));
    // 1. if you enter (), it just uses panic! with a mesage.
    // - So you could actually just write panic! instead of todo! and it would be the same.
    ($($arg:tt)+) => (panic!("not yet implemented: {}", $crate::format_args!($($arg)+)))
    // 2. If you enter some arguments, it will try to print them
    // - You can see the same format_args! macro inside, which works like println!
}

macro_rules! my_macro {
    () => {
        println!("Let's print this.");
    };
    ($input:expr) => {
        println!("Single expression.");
        my_macro!(); // invoke it self
    };
    ($($input:expr),*) => {
        println!("Multiple expression.");
        my_macro!();
    };
}

macro_rules! dbg_rep {
    () => {
        $crate::eprintln!("[{}:{}]", $crate::file!(), $crate::line!());
        // $crate means the crate that it's in.
    };
    ($val:expr) => {
        // Use of `match` here is intentional because it affects the lifetimes
        match $val {
            // tmp = vec![...]
            tmp => {
                // eprintln! : same as println! except it prints to `io::stderr` instead of stdout
                $crate::eprintln!(
                    "[{}:{}] {} = {:#?}",
                    $crate::file!(),
                    $crate::line!(),
                    $crate::stringify!($val),
                    &tmp
                );
                tmp
            }
        }
    };
    // Trailing comma with single argument is ignored (just invode previous arm)
    ($val:expr,) => {
        $crate::dbg!($val)
    };
    // one or more
    ($($val:expr),+ $(,)?) => {
        ($($crate::dbg!($val)),+,)
    };
}

fn not_done() {
    let time = 8;
    let reason = "lack of time";
    todo!(
        "Not done yet become of {}. Check back in {} hours",
        reason,
        time
    );
}

fn main() {
    // make_a_function!(print_it, 5, 5, 6, I); // We want a fucntion called print_it() that prints
    //                                         // everything else we give it
    // print_it();
    // make_a_function!(say_its_nice, this, is, really, nice); // Same here but we change the function
    // say_its_nice();
    //
    // not_done(); // It will panic

    // my_macro!(vec![8, 9, 0]); // reference in stack and data in heap
    // my_macro!(tohetch); // single data in stack
    // my_macro!(8, 7, 0, 10); // multiple data in stack
    // my_macro!();

    dbg!(); // no args
    dbg!(vec![8, 9, 10]); // one expression arg
}
