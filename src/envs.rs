use std::env::args;
use std::io;

enum Letters {
    Capitalize,
    Lowercase,
    Nothing,
}

fn io_main() {
    println!("Please type something, or x to escape:");
    let mut input_string = String::new();

    // This is the part that doesn't work right => you need to trim it
    // while input_string != "x\n" {
    while input_string.trim() != "x" {
        // clear the String. Otherwise it will keep adding to it
        input_string.clear();
        // Get the stdin fromthe user, and put it in read_string
        io::stdin().read_line(&mut input_string).unwrap();
        println!("You wrote {:?}", input_string);
        // println!("You wrote {}", dbg!(&input_string));
    }
    println!("See you later!");
    // println!("{:?}", std::env::args());
    // print all user input at the command
    // args == iterator
}
fn arg_main() {
    let input = args();

    // for entry in input {
    //     println!("You entered: {}", entry);
    // }

    // Usually skip first element (name of program)
    input.skip(1).for_each(|item| {
        println!("You wrote {}", item);
    })
}
fn main() {
    // let mut changes = Letters::Nothing;
    // let input = args().collect::<Vec<_>>();
    //
    // if input.len() > 2 {
    //     match input[1].as_str() {
    //         "capital" => changes = Letters::Capitalize,
    //         "lowercase" => changes = Letters::Lowercase,
    //         _ => {}
    //     }
    // }
    //
    // // skip two elements at the beginning
    // for word in input.iter().skip(2) {
    //     match changes {
    //         Letters::Capitalize => println!("{}", word.to_uppercase()),
    //         Letters::Lowercase => println!("{}", word.to_lowercase()),
    //         _ => println!("{}", word),
    //     }
    // }

    for item in std::env::vars() {
        println!("{:?}", item);
    }

    // get one env variable
    println!("{}", env!("USER")); // Use if you want panic
    println!("{}", option_env!("ROOT").unwrap_or("Can't find ROOT"));
    println!("{}", option_env!("CARGO").unwrap_or("Can't find CARGO"));
}
