//============================================================
// fn returns_reference() -> &'static str {
//     // &'static str => Return String literal
//     let my_string = String::from("I am a string"); // this String destroy when this function return.
//                                                    // &my_string // You can't access from outside of function..
//     "I am a str" // isn't borrowed from anything
// }

//============================================================
// #[derive(Debug)]
// struct City {
//     name: &'static str, // need lifetime because &str is reference
//     date_founded: u32,
// }

//============================================================
// #[derive(Debug)]
// struct City<'city> {
//     // City has lifetime 'city
//     name: &'city str, // and name also has lifetime 'city - this will destroy when City struct is destroy
//     date_founded: u32,
// }

//============================================================
// struct Adventurer<'a> {
//     name: &'a str,
//     hit_points: u32,
// }

// impl Adventurer<'_> {
//     // <'_> - When you implement struct that has reference, you must tell to Rust this have a reference. ( which elided <'a> in this case )
//     fn take_damage(&mut self) {
//         self.hit_points -= 20;
//         println!("{} has {} hit points left!", self.name, self.hit_points);
//     }
// }

//============================================================
struct Adventurer<'a> {
    name: &'a str, // reference in Struct => need to write lifetime
    hit_points: u32,
}

impl Adventurer<'_> {
    fn take_damage(&mut self) {
        self.hit_points -= 20;
        println!("{} has {} hit points left!", self.name, self.hit_points);
        // name => reference in Struct => need to write lifetime
    }
}

impl std::fmt::Display for Adventurer<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} has {} hit points.", self.name, self.hit_points)
        // name => reference in Struct => need to write lifetime
        // You must write lifetime wherever references are used.
    }
}

fn main() {
    // let my_str = returns_reference();
    // println!("{}", my_str);

    // let city_names = vec!["Ichinomiya".to_string(), "Kurume".to_string()];

    // let my_city = City {
    //     // name: "Ichinomiya",
    //     name: &city_names[0], // This is a &str, but not a &'static str. It is a reference to a value inside city_names.
    //     date_founded: 1921,
    // };

    // println!("{} was founded in {}", my_city.name, my_city.date_founded);

    let mut billy = Adventurer {
        name: "Billy",
        hit_points: 100_000,
    };
    println!("{}", billy);
    billy.take_damage();
}
